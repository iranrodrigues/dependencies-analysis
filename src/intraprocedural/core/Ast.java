package intraprocedural.core;

import interprocedural.visitor.PresenceConditionVisitor;

import java.io.File;
import java.util.ArrayList;

import tree.Node;
import tree.TranslationUnit;
import tree.visitor.VisitorASTOrganizer;
import core.ASTGenerator;
import de.fosd.typechef.FrontendOptions;
import de.fosd.typechef.FrontendOptionsWithConfigFiles;
import de.fosd.typechef.Lex;
import de.fosd.typechef.lexer.options.OptionException;
import de.fosd.typechef.parser.TokenReader;
import de.fosd.typechef.parser.c.AST;
import de.fosd.typechef.parser.c.CParser;
import de.fosd.typechef.parser.c.CToken;
import de.fosd.typechef.parser.c.CTypeContext;
import de.fosd.typechef.parser.c.ParserMain;

public class Ast {
	
	private File source;
	private File stubs;
	private Node node;

	public Ast(File source, File stubs) {
		setSource(source);
		setStubs(stubs);
	}

	public File getSource() {
		return source;
	}

	public void setSource(File source) {
		this.source = source;
	}

	public File getStubs() {
		return stubs;
	}

	public void setStubs(File stubs) {
		this.stubs = stubs;
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}
	
	public enum GenerationStatus { OK, OPTIONS_EXCEPTION, BAD_AST }

	public GenerationStatus generate() {
		FrontendOptions myParserOptions = new FrontendOptionsWithConfigFiles();
		ArrayList<String> parameters = new ArrayList<String>();

		//parameters.add("-o");
		//parameters.add("interprocedural\\bison2.0\\analysis\\src\\");
		//parameters.add("--serializeAST");
		//parameters.add("--reuseAST");
		//parameters.add("--parserstatistics");
		//parameters.add("--typecheck");
		parameters.add("--lexNoStdout");
		parameters.add("-h");
		parameters.add(this.stubs.getPath());
		parameters.add(this.source.getPath());

		String[] parameterArray = parameters
				.toArray(new String[parameters.size()]);
		
		try {
			System.out.print("Trying to parse options for file " + getSource().getPath() + "... ");
			myParserOptions.parseOptions(parameterArray);
		} catch (OptionException e) {
			System.out.println("ERROR");
			e.printStackTrace();
			return GenerationStatus.OPTIONS_EXCEPTION;
		}
		System.out.println("OK");

		ParserMain parser = new ParserMain(new CParser(null, false));

		TokenReader<CToken, CTypeContext> in = Lex.lex(myParserOptions);
		System.out.println("Parsing AST...");
		AST ast = parser.parserMain(in, myParserOptions);
		Node myAst = new TranslationUnit();
		
		try {
			System.out.print("Trying to generate AST for file " + getSource().getPath() + "... ");
			new ASTGenerator().generate(ast, myAst);
		} catch (Exception e) {
			System.out.println("ERROR");
			e.printStackTrace();
			return GenerationStatus.BAD_AST;
		}
		System.out.println("OK");
		// Optimize AST
		myAst.accept(new VisitorASTOrganizer());
		// Get the presence condition for all nodes of the tree
		myAst.accept(new PresenceConditionVisitor());
		setNode(myAst);
		return GenerationStatus.OK;
	}
	
}
