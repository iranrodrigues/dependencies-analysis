package intraprocedural.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tree.AtomicNamedDeclarator;
import tree.FunctionDef;
import tree.Id;
import tree.Opt;

public class Function {
	private FunctionDef functionDef;
	private String name;
	private Set<Id> localVariables = new HashSet<Id>();
	private Set<Opt> directives = new HashSet<Opt>();
	private Set<Opt> opts = new HashSet<Opt>();
	private List<IntraproceduralDependency> dependencies = new ArrayList<IntraproceduralDependency>();
	
	public Function(FunctionDef functionDef) {
		this.functionDef = functionDef;
		
		for (int i = 0; i < functionDef.getChildren().size(); i++) {
			if (functionDef.getChildren().get(i) instanceof AtomicNamedDeclarator) {
				for (int j = 0; j < functionDef.getChildren().get(i).getChildren().size(); j++) {
					if (functionDef.getChildren().get(i).getChildren().get(j) instanceof Id) {
						this.name = ((Id) functionDef.getChildren().get(i).getChildren().get(j)).getName();
					}
				}
			}
		}
	}

	public FunctionDef getFunctionDef() {
		return functionDef;
	}

	public void setFunctionDef(FunctionDef functionDef) {
		this.functionDef = functionDef;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Id> getLocalVariables() {
		return localVariables;
	}

	public void setLocalVariables(Set<Id> localVariables) {
		this.localVariables = localVariables;
	}

	public Set<Opt> getDirectives() {
		return directives;
	}

	public void setDirectives(Set<Opt> directives) {
		this.directives = directives;
	}
	
	public Set<Opt> getOpts() {
		return opts;
	}

	public void setOpts(Set<Opt> opts) {
		this.opts = opts;
	}

	public List<IntraproceduralDependency> getDependencies() {
		return dependencies;
	}

	public void setDependencies(List<IntraproceduralDependency> dependencies) {
		this.dependencies = dependencies;
	}
}
