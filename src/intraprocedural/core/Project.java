package intraprocedural.core;

import intraprocedural.core.Ast.GenerationStatus;
import intraprocedural.visitor.FindFunctionDirectivesVisitor;
import intraprocedural.visitor.FindFunctionsVisitor;
import intraprocedural.visitor.FindLocalVariableUsesVisitor;
import intraprocedural.visitor.FindVariableDeclarationVisitor;
import intraprocedural.visitor.FindVariableDefinitionVisitor;
import intraprocedural.visitor.FindVariableScopeVisitor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import tree.FunctionDef;
import tree.Id;
import tree.Node;
import tree.Opt;

public class Project {
	
	private String sourcePath;
	private String stubsPath;
	private int totalFiles = 0;
	private int totalSuccessfulFiles = 0;
	private Map<String, String> skippedFiles = new HashMap<String, String>();
	private Set<String> blacklist = new HashSet<String>();
	private List<Ast> asts = new ArrayList<Ast>();
	private Map<FunctionDef, Function> functions = new HashMap<FunctionDef, Function>();
	private List<IntraproceduralDependency> dependencies = new ArrayList<IntraproceduralDependency>();
	
	public Project(String sourcePath, String stubsPath) {
		setSourcePath(sourcePath);
		setStubsPath(stubsPath);
	}

	public String getSourcePath() {
		return sourcePath;
	}

	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	public String getStubsPath() {
		return stubsPath;
	}

	public void setStubsPath(String stubsPath) {
		this.stubsPath = stubsPath;
	}
	
	public int getTotalFiles() {
		return totalFiles;
	}

	public void setTotalFiles(int totalFiles) {
		this.totalFiles = totalFiles;
	}

	public int getTotalSuccessfulFiles() {
		return totalSuccessfulFiles;
	}

	public void setTotalSuccessfulFiles(int totalSuccessfulFiles) {
		this.totalSuccessfulFiles = totalSuccessfulFiles;
	}
	
	public Map<String, String> getSkippedFiles() {
		return skippedFiles;
	}

	public void setSkippedFiles(Map<String, String> skippedFiles) {
		this.skippedFiles = skippedFiles;
	}

	public Set<String> getBlacklist() {
		return blacklist;
	}

	public void setBlacklist(Set<String> blacklist) {
		this.blacklist = blacklist;
	}

	public List<Ast> getAsts() {
		return asts;
	}

	public void setAsts(List<Ast> asts) {
		this.asts = asts;
	}

	public Map<FunctionDef, Function> getFunctions() {
		return functions;
	}

	public void setFunctions(Map<FunctionDef, Function> functions) {
		this.functions = functions;
	}
	
	public List<IntraproceduralDependency> getDependencies() {
		return dependencies;
	}

	public void setDependencies(List<IntraproceduralDependency> dependencies) {
		this.dependencies = dependencies;
	}

	public void getFile(File path) {
		if (path.isDirectory()) {
			for (File file : path.listFiles()) {
				getFile(file);
			}
		} else {
			if (!path.getName().startsWith(".")) {
				if (path.getName().endsWith(".c")) {
						//|| path.getName().endsWith(".h")) {
					System.out.println("FILE: " + path.getAbsolutePath());
					setTotalFiles(getTotalFiles() + 1);
					if (getBlacklist().contains(path.getPath())) {
						getSkippedFiles().put(path.getPath(), "File is blacklisted");
						System.out.println("File is blacklisted.");
						return;
					}
					Ast ast = new Ast(path, new File(getStubsPath()));
					GenerationStatus generationStatus;
					do {
						generationStatus = ast.generate();
						/*if (generationStatus == GenerationStatus.BAD_AST) {
							System.exit(0);
						}*/
					} while (generationStatus == GenerationStatus.OPTIONS_EXCEPTION);
					if (generationStatus == GenerationStatus.OK) {
						setTotalSuccessfulFiles(getTotalSuccessfulFiles() + 1);
						getAsts().add(ast);
						//Node myAst = ast.getNode();
						/*FindVariableDefinitionVisitor findVariableDefinitionVisitor = new FindVariableDefinitionVisitor();
						myAst.accept(findVariableDefinitionVisitor);
						for (Id id : findVariableDefinitionVisitor.getLocalVariables()) {
							System.out.println("Variable definition: " + id.getName() + " at " + id.getPositionFrom().getLine() + ":" + id.getPositionFrom().getColumn());
						}*/
						//myAst.accept(new VisitorPrinter(false));
						//myAst.accept(new VisitorPrinterNames());
					} else {
						getSkippedFiles().put(path.getPath(), generationStatus.toString());
					}
				}
			}
		}
	}
	
	public void loadBlacklist() {
		File f = new File(sourcePath + "..\\blacklist.txt");
		if (f.exists()) {
			try {
				setBlacklist(new HashSet<String>(FileUtils.readLines(f)));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void analyze() {
		Map<FunctionDef, Function> functions = new HashMap<FunctionDef, Function>();
		// Get blacklisted files (files too messy to be parsed)
		loadBlacklist();
		// Parse every .c file in the path
		getFile(new File(sourcePath));
		// Cycle through every AST
		System.out.println("ASTs: " + asts.size());
		for (Ast ast : this.asts) {
			// Get all functions in this AST
			functions = findFunctions(ast);
			// Cycle through all functions
			System.out.println("Functions in this AST: " + functions.size());
			for (Entry<FunctionDef, Function> functionEntry : functions.entrySet()) {
				Function function = functionEntry.getValue();
				// Only functions which have directives can have intraprocedural dependencies
				if (function.getDirectives().isEmpty()) {
					System.out.println("Function " + function.getName() + " skipped!");
					continue;
				}
				/* This code do not detect all definitions - only declarations
				// Get all variable declarations inside the function
				Set<Id> variables = findVariableDeclarations(function);
				*/
				
				// Get all variable definitions inside the function
				Set<Id> variables = findVariableDefinitions(function);
				
				// Cycle through every variable declaration
				System.out.println("Variables in function " + function.getName() + ": " + variables.size());
				for (Id variable : variables) {
					// Get all intraprocedural dependencies regarding this variable 
					List<IntraproceduralDependency> intraproceduralDependencies = findIntraproceduralDependencies(function, variable);
					// Assign the dependencies to the function
					function.getDependencies().addAll(intraproceduralDependencies);
					// Assign the dependencies to the project
					this.dependencies.addAll(intraproceduralDependencies);
				}
			}
			this.functions.putAll(functions);
		}
	}

	public Map<FunctionDef, Function> findFunctions(Ast ast) {
		Map<FunctionDef, Function> functions;
		
		Node myAst = ast.getNode();
		FindFunctionsVisitor findFunctionsVisitor = new FindFunctionsVisitor();
		myAst.accept(findFunctionsVisitor);
		// Get all function definitions on this AST
		List<FunctionDef> functionDefs = findFunctionsVisitor.getFunctions();
		functions = new HashMap<FunctionDef, Function>();
		// Create a function object for each function definition
		for (FunctionDef functionDef : functionDefs) {
			FindFunctionDirectivesVisitor findFunctionDirectivesVisitor = new FindFunctionDirectivesVisitor(functionDef);
			functionDef.accept(findFunctionDirectivesVisitor);
			// Get all function directives
			Set<Opt> functionDirectives = findFunctionDirectivesVisitor.getFunctionDirectives();
			// Get function inner directives (ifdefs inside function)
			Set<Opt> functionOpts = findFunctionDirectivesVisitor.getFunctionOpts();
			
			Function function = new Function(functionDef);
			function.setDirectives(functionDirectives);
			function.setOpts(functionOpts);
			functions.put(functionDef, function);
		}
		return functions;
	}
	
	public Set<Id> findVariableDeclarations(Function function) {
		FindVariableDeclarationVisitor findVariableDeclarationVisitor = new FindVariableDeclarationVisitor();
		function.getFunctionDef().accept(findVariableDeclarationVisitor);
		Set<Id> localVariables = findVariableDeclarationVisitor.getLocalVariables();
		function.setLocalVariables(localVariables);
		return localVariables;
	}
	
	public Set<Id> findVariableDefinitions(Function function) {
		FindVariableDefinitionVisitor findVariableDefinitionVisitor = new FindVariableDefinitionVisitor();
		function.getFunctionDef().accept(findVariableDefinitionVisitor);
		Set<Id> variablesDefinitions = findVariableDefinitionVisitor.getLocalVariables();
		Set<Id> localVariablesDefinitions = new HashSet<Id>();
		
		FindVariableDeclarationVisitor findVariableDeclarationVisitor = new FindVariableDeclarationVisitor();
		function.getFunctionDef().accept(findVariableDeclarationVisitor);
		Set<String> localVariablesDeclarations = findVariableDeclarationVisitor.getLocalVariablesNames();
		
		for (Id variable : variablesDefinitions) {
			if (localVariablesDeclarations.contains(variable.getName())) {
				localVariablesDefinitions.add(variable);
			}
		}
		
		function.setLocalVariables(localVariablesDefinitions);
		return localVariablesDefinitions;
	}
	
	public List<IntraproceduralDependency> findIntraproceduralDependencies(Function function, Id variable) {
		FindLocalVariableUsesVisitor findLocalVariableUsesVisitor = new FindLocalVariableUsesVisitor(variable);
		FindVariableScopeVisitor findVariableScopeVisitor = new FindVariableScopeVisitor();
		variable.accept(findVariableScopeVisitor);
		if (findVariableScopeVisitor.getCompoundStatement() == null) {
			System.out.println(function.getName() + " / " + variable.getName());
			return new ArrayList<IntraproceduralDependency>();
		}
		findVariableScopeVisitor.getCompoundStatement().accept(findLocalVariableUsesVisitor);
		List<Id> localVariableUses = findLocalVariableUsesVisitor.getLocalVariableUses();
		System.out.println("Uses of variable " + variable.getName() + ": " + localVariableUses.size());
		List<IntraproceduralDependency> intraproceduralDependencies = new ArrayList<IntraproceduralDependency>();
		for (Id localVariableUse : localVariableUses) {
			if (!(localVariableUse.getPresenceCondition().equivalentTo(variable.getPresenceCondition())) && // non equivalent (same) presence condition
			   (!(localVariableUse.getPresenceCondition().and(variable.getPresenceCondition()).isContradiction()))) { // presence condition in declaration *and* use cannot be a contradiction
				IntraproceduralDependency dependency = new IntraproceduralDependency(function, variable.getPresenceCondition().and(localVariableUse.getPresenceCondition()), variable, localVariableUse);
				intraproceduralDependencies.add(dependency);
			}
		}
		System.out.println("Dependencies of variable " + variable.getName() + ": " + intraproceduralDependencies.size());
		return intraproceduralDependencies;
	}

	public void generateMetrics() {
		
	}	
	
}
