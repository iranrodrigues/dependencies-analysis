package intraprocedural.core;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map.Entry;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import tree.FunctionDef;

public class POISpreadsheet {

	Project project;
	String filename;
	Workbook workbook = new XSSFWorkbook();
	
	interface CellNames {
		public static final String TOTAL_FUNCTIONS = "'Summary'!B1";
		public static final String TOTAL_FUNCTIONS_WITH_DIRECTIVES = "'Summary'!B2";
		public static final String TOTAL_FUNCTIONS_WITH_DEPENDENCIES = "'Summary'!B3";
		
		public static final String TOTAL_DEPENDENCIES = "'Summary'!B5";
		public static final String TOTAL_MANDATORY_OPTIONAL_DEPENDENCIES = "'Summary'!B6";
		public static final String TOTAL_OPTIONAL_MANDATORY_DEPENDENCIES = "'Summary'!B7";
		public static final String TOTAL_OPTIONAL_OPTIONAL_DEPENDENCIES = "'Summary'!B8";
		
		public static final String TOTAL_FILES = "'Summary'!B10";
		public static final String TOTAL_SUCCESSFUL_FILES = "'Summary'!B11";
		
		public static final String DEPENDENCY_DIRECTIONS = "'Dependencies'!A2:A1048576";
	}
	
	public POISpreadsheet(Project project, String filename) {
		System.out.println("Creating spreadsheet...");
		setProject(project);
		setFilename(filename);
		
		System.out.println("Creating sheet Functions...");
		createSheetFunctions();
		System.out.println("Creating sheet Dependencies...");
		createSheetDependencies();
		System.out.println("Creating sheet Skipped Files...");
		createSheetSkippedFiles();
		System.out.println("Creating sheet Summary...");
		createSheetSummary();
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Workbook getWorkbook() {
		return workbook;
	}

	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}
	
	public void createSheetFunctions() {
		Sheet sheet = getWorkbook().createSheet("Functions");

		// Inserting header
		String[] headers = {"FILE", "FUNCTION", "HAS_DIRECTIVES", "DEPENDENCIES"};
		// Create row
		Row row = sheet.createRow(0);
		for (int i = 0; i < headers.length; i++) {
			// Create cells on row
			row.createCell(i).setCellValue(headers[i]);
		}
		int i = 1;
		for (Entry<FunctionDef, Function> functionMap : getProject().getFunctions().entrySet()) {
			row = sheet.createRow(i);
			FunctionDef functionDef = functionMap.getKey();
			Function function = functionMap.getValue();
			// Filename
			row.createCell(0).setCellValue(functionDef.getPositionFrom().getFile().substring(5));
			// Function name
			row.createCell(1).setCellValue(function.getName());
			// Has inner directives?
			row.createCell(2).setCellValue(new Boolean(function.getOpts().size() > 0).toString());
			// Total of dependencies
			row.createCell(3).setCellValue(function.getDependencies().size());
			i++;
			if (i > 1048575) {
				break;
			}
		}
	}
	
	public void createSheetDependencies() {
		Sheet sheet = getWorkbook().createSheet("Dependencies");

		// Inserting header
		String[] headers = {"DIRECTION", "TYPE", "PRESENCE_CONDITION", "DEFINITION_PC", "USE_PC", "FUNCTION", "VARIABLE", "FILENAME", "DEFINITION_POSITION", "USE_POSITION"};
		// Create row
		Row row = sheet.createRow(0);
		for (int i = 0; i < headers.length; i++) {
			row.createCell(i).setCellValue(headers[i]);
		}
		
		int i = 1;
		for (IntraproceduralDependency dependency : getProject().getDependencies()) {
		/*List<IntraproceduralDependency> dependencies = new ArrayList<IntraproceduralDependency>();
		for (Entry<FunctionDef, Function> functionMap : getProject().getFunctions().entrySet()) {
			Function function = functionMap.getValue();
			dependencies.addAll(function.getDependencies());
		}
		for (IntraproceduralDependency dependency : dependencies) {*/
			// toString() returns all dependency metrics
			String[] data = dependency.toString().split(";");
			row = sheet.createRow(i);
			for (int j = 0; j < data.length; j++) {
				if (data[j].length() > 32766) {
					row.createCell(j).setCellValue(data[j].substring(0,32766));
				} else {
					row.createCell(j).setCellValue(data[j]);
				}
			}
			i++;
			if (i > 1048575) {
				break;
			}
		}
	}
	
	public void createSheetSkippedFiles() {
		Sheet sheet = getWorkbook().createSheet("Skipped Files");

		// Inserting header
		String[] headers = {"FILE", "REASON"};
		// Create row
		Row row = sheet.createRow(0);
		for (int i = 0; i < headers.length; i++) {
			// Create cells on row
			row.createCell(i).setCellValue(headers[i]);
		}
		int i = 1;
		for (Entry<String, String> skippedFileMap : getProject().getSkippedFiles().entrySet()) {
			row = sheet.createRow(i);
			String filename = skippedFileMap.getKey();
			String reason = skippedFileMap.getValue();
			// Filename
			row.createCell(0).setCellValue(filename);
			// Reason to be skipped
			row.createCell(1).setCellValue(reason);
			i++;
			if (i > 1048575) {
				break;
			}
		}
	}
	
	public void createSheetSummary() {
		Cell cell;
		Sheet sheet = getWorkbook().createSheet("Summary");
		CellStyle percentageStyle = getWorkbook().createCellStyle();
		percentageStyle.setDataFormat(getWorkbook().createDataFormat().getFormat("0.000%"));
		
		int totalFunctions = this.project.getFunctions().size();
		int totalFunctionsWithDirectives = 0;
		int totalFunctionsWithDependencies = 0;
		int totalDependencies = this.project.getDependencies().size();
		
		// Cycle through all functions
		for (Entry<FunctionDef, Function> functionMap : getProject().getFunctions().entrySet()) {
			Function function = functionMap.getValue();
			// Does the function have directives?
			if (!(function.getOpts().isEmpty())) {
				totalFunctionsWithDirectives++;
				// Does the function have dependencies?
				if (!(function.getDependencies().isEmpty())) {
					totalFunctionsWithDependencies++;
				}
			}
		}
		
		int i = 0;
		
		Row row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of functions");
		row.createCell(1).setCellValue(totalFunctions);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of functions with directives");
		row.createCell(1).setCellValue(totalFunctionsWithDirectives);
		cell = row.createCell(2);
		// Percentage of functions with directives out of total
		cell.setCellFormula("IF(" + CellNames.TOTAL_FUNCTIONS + "=0, 0, " + CellNames.TOTAL_FUNCTIONS_WITH_DIRECTIVES + "/" + CellNames.TOTAL_FUNCTIONS + ")");
		cell.setCellStyle(percentageStyle);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of functions with dependencies");
		row.createCell(1).setCellValue(totalFunctionsWithDependencies);
		cell = row.createCell(2);
		// Percentage of functions with dependencies
		cell.setCellFormula("IF(" + CellNames.TOTAL_FUNCTIONS + "=0, 0, " + CellNames.TOTAL_FUNCTIONS_WITH_DEPENDENCIES + "/" + CellNames.TOTAL_FUNCTIONS + ")");
		cell.setCellStyle(percentageStyle);
		cell = row.createCell(3);
		// Percentage of functions with dependencies out of total with directives
		cell.setCellFormula("IF(" + CellNames.TOTAL_FUNCTIONS_WITH_DIRECTIVES + "=0, 0, " + CellNames.TOTAL_FUNCTIONS_WITH_DEPENDENCIES + "/" + CellNames.TOTAL_FUNCTIONS_WITH_DIRECTIVES + ")");
		cell.setCellStyle(percentageStyle);
		
		i++;
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of dependencies");
		row.createCell(1).setCellValue(totalDependencies);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Mandatory<->Optional");
		// Count occurrences of Mandatory<->Optional
		row.createCell(1).setCellFormula("COUNTIF(" + CellNames.DEPENDENCY_DIRECTIONS + ", \"Mandatory<->Optional\")");
		cell = row.createCell(2);
		// Percentage of Mandatory<->Optional dependencies out of total
		cell.setCellFormula("IF(" + CellNames.TOTAL_DEPENDENCIES + "=0, 0, " + CellNames.TOTAL_MANDATORY_OPTIONAL_DEPENDENCIES + "/" + CellNames.TOTAL_DEPENDENCIES + ")");
		cell.setCellStyle(percentageStyle);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Optional<->Mandatory");
		// Count occurrences of Optional<->Mandatory
		row.createCell(1).setCellFormula("COUNTIF(" + CellNames.DEPENDENCY_DIRECTIONS + ", \"Optional<->Mandatory\")");
		cell = row.createCell(2);
		// Percentage of Optional<->Mandatory dependencies out of total
		cell.setCellFormula("IF(" + CellNames.TOTAL_DEPENDENCIES + "=0, 0, " + CellNames.TOTAL_OPTIONAL_MANDATORY_DEPENDENCIES + "/" + CellNames.TOTAL_DEPENDENCIES + ")");
		cell.setCellStyle(percentageStyle);
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Optional<->Optional");
		// Count occurrences of Optional<->Optional
		row.createCell(1).setCellFormula("COUNTIF(" + CellNames.DEPENDENCY_DIRECTIONS + ", \"Optional<->Optional\")");
		cell = row.createCell(2);
		// Percentage of Optional<->Optional dependencies out of total
		cell.setCellFormula("IF(" + CellNames.TOTAL_DEPENDENCIES + "=0, 0, " + CellNames.TOTAL_OPTIONAL_OPTIONAL_DEPENDENCIES + "/" + CellNames.TOTAL_DEPENDENCIES + ")");
		cell.setCellStyle(percentageStyle);
		
		i++;		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of files");
		row.createCell(1).setCellValue(getProject().getTotalFiles());
		
		row = sheet.createRow(i++);
		row.createCell(0).setCellValue("Total of successfully analyzed files");
		row.createCell(1).setCellValue(getProject().getTotalSuccessfulFiles());
		cell = row.createCell(2);
		// Percentage of successful files out of total
		cell.setCellFormula("IF(" + CellNames.TOTAL_FILES + "=0, 0, " + CellNames.TOTAL_SUCCESSFUL_FILES + "/" + CellNames.TOTAL_FILES + ")");
		cell.setCellStyle(percentageStyle);
	}

	public void write() {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(new File(getFilename()));
			getWorkbook().write(fos);
			 
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error on writing to file " + getFilename());
		} finally {
			try {
				fos.flush();
				fos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Spreadsheet created!");
	}
	
}
