package intraprocedural.core;

import tree.Id;
import de.fosd.typechef.featureexpr.FeatureExpr;

public class IntraproceduralDependency {
	private Function function;
	private FeatureExpr presenceCondition;
	private Id definition;
	private Id use;
	
	public IntraproceduralDependency(Function function, FeatureExpr presenceCondition, Id definition, Id use) {
		this.function = function;
		this.presenceCondition = presenceCondition;
		this.definition = definition;
		this.use = use;
	}
	
	public Function getFunction() {
		return function;
	}
	
	public FeatureExpr getPresenceCondition() {
		return presenceCondition;
	}
	
	public Id getDefinition() {
		return definition;
	}
	
	public Id getUse() {
		return use;
	}
	
	public String getDirection() {
		if (definition.getPresenceCondition().isTautology()) {
			return "Mandatory<->Optional";
		} else if (use.getPresenceCondition().isTautology()) {
			return "Optional<->Mandatory";
		} else {
			return "Optional<->Optional";
		}
	}
	
	public String getFile() {
		return this.definition.getPositionFrom().getFile().substring(5);
	}
	
	public String getDefinitionPosition() {
		return this.definition.getPositionFrom().getLine() + ":" + this.definition.getPositionFrom().getColumn();
	}
	
	public String getUsePosition() {
		return this.use.getPositionFrom().getLine() + ":" + this.definition.getPositionFrom().getColumn();
	}
	
	public String toString() {
		return
			getDirection() + ";" +
			getClass().getSimpleName() + ";" +
			getPresenceCondition().toString() + ";" +
			definition.getPresenceCondition().toString() + ";" +
			use.getPresenceCondition().toString() + ";" +
			function.getName() + ";" +
			definition.getName() + ";" +
			getFile() + ";" +
			getDefinitionPosition() + ";" +
			getUsePosition();
	}
	
}
