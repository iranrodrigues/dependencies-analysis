package interprocedural.core;

import interprocedural.core.Ast.GenerationStatus;
import interprocedural.visitor.FindFunctionCallVariablesVisitor;
import interprocedural.visitor.FindFunctionCallsVisitor;
import interprocedural.visitor.FindFunctionDefVisitor;
import interprocedural.visitor.FindFunctionDirectivesVisitor;
import interprocedural.visitor.FindFunctionParameterUsesVisitor;
import interprocedural.visitor.FindFunctionParametersVisitor;
import interprocedural.visitor.FindFunctionsVisitor;
import interprocedural.visitor.FindGlobalVariableUsesVisitor;
import interprocedural.visitor.FindGlobalVariablesDeclarationsVisitor;
import interprocedural.visitor.FindVariableDeclarationVisitor;
import interprocedural.visitor.FindVariableFunctionVisitor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import tree.CompoundStatement;
import tree.FunctionCall;
import tree.FunctionDef;
import tree.Id;
import tree.Node;
import tree.Opt;

public class Project {
	
	private String sourcePath;
	private String stubsPath;
	private int totalFiles = 0;
	private int totalSuccessfulFiles = 0;
	private Map<String, String> skippedFiles = new HashMap<String, String>();
	private Set<String> blacklist = new HashSet<String>();
	private List<Ast> asts = new ArrayList<Ast>();
	private Map<FunctionDef, Function> functions = new HashMap<FunctionDef, Function>();
	private List<Dependency> dependencies = new ArrayList<Dependency>();
	private Map<String, Map<String, Set<String>>> maintenancePoints = new HashMap<String, Map<String, Set<String>>>();
	private Map<String, Map<String, Set<String>>> impactPoints = new HashMap<String, Map<String, Set<String>>>();
	private Set<Map<Function, Map<Function, Integer>>> dependencyDepths = new HashSet<Map<Function, Map<Function, Integer>>>();
	private List<Chain> chainedFunctions = new ArrayList<Chain>();
	
	public Project(String sourcePath, String stubsPath) {
		setSourcePath(sourcePath);
		setStubsPath(stubsPath);
	}

	public String getSourcePath() {
		return sourcePath;
	}

	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	public String getStubsPath() {
		return stubsPath;
	}

	public void setStubsPath(String stubsPath) {
		this.stubsPath = stubsPath;
	}
	
	public int getTotalFiles() {
		return totalFiles;
	}

	public void setTotalFiles(int totalFiles) {
		this.totalFiles = totalFiles;
	}

	public int getTotalSuccessfulFiles() {
		return totalSuccessfulFiles;
	}

	public void setTotalSuccessfulFiles(int totalSuccessfulFiles) {
		this.totalSuccessfulFiles = totalSuccessfulFiles;
	}
	
	public Map<String, String> getSkippedFiles() {
		return skippedFiles;
	}

	public void setSkippedFiles(Map<String, String> skippedFiles) {
		this.skippedFiles = skippedFiles;
	}

	public Set<String> getBlacklist() {
		return blacklist;
	}

	public void setBlacklist(Set<String> blacklist) {
		this.blacklist = blacklist;
	}

	public List<Ast> getAsts() {
		return asts;
	}

	public void setAsts(List<Ast> asts) {
		this.asts = asts;
	}

	public Map<FunctionDef, Function> getFunctions() {
		return functions;
	}

	public void setFunctions(Map<FunctionDef, Function> functions) {
		this.functions = functions;
	}

	public List<Dependency> getDependencies() {
		return dependencies;
	}

	public void setDependencies(List<Dependency> dependencies) {
		this.dependencies = dependencies;
	}

	public Map<String, Map<String, Set<String>>> getMaintenancePoints() {
		return maintenancePoints;
	}

	public void setMaintenancePoints(Map<String, Map<String, Set<String>>> maintenancePoints) {
		this.maintenancePoints = maintenancePoints;
	}

	public Map<String, Map<String, Set<String>>> getImpactPoints() {
		return impactPoints;
	}

	public void setImpactPoints(Map<String, Map<String, Set<String>>> impactPoints) {
		this.impactPoints = impactPoints;
	}

	public Set<Map<Function, Map<Function, Integer>>> getDependencyDepths() {
		return dependencyDepths;
	}

	public void setDependencyDepths(
			Set<Map<Function, Map<Function, Integer>>> dependencyDepths) {
		this.dependencyDepths = dependencyDepths;
	}

	public List<Chain> getChainedFunctions() {
		return chainedFunctions;
	}

	public void setChainedFunctions(List<Chain> chainedFunctions) {
		this.chainedFunctions = chainedFunctions;
	}

	public void getFile(File path) {
		if (path.isDirectory()) {
			for (File file : path.listFiles()) {
				getFile(file);
			}
		} else {
			if (!path.getName().startsWith(".")) {
				if (path.getName().endsWith(".c")) {
						//|| path.getName().endsWith(".h")) {
					System.out.println("FILE: " + path.getAbsolutePath());
					setTotalFiles(getTotalFiles() + 1);
					if (getBlacklist().contains(path.getPath())) {
						getSkippedFiles().put(path.getPath(), "File is blacklisted");
						System.out.println("File is blacklisted.");
						return;
					}
					Ast ast = new Ast(path, new File(getStubsPath()));
					GenerationStatus generationStatus;
					do {
						generationStatus = ast.generate();
					} while (generationStatus == GenerationStatus.OPTIONS_EXCEPTION);
					if (generationStatus == GenerationStatus.OK) {
						setTotalSuccessfulFiles(getTotalSuccessfulFiles() + 1);
						getAsts().add(ast);
					} else {
						getSkippedFiles().put(path.getPath(), generationStatus.toString());
					}
				}
			}
		}
	}
	
	public void loadBlacklist() {
		File f = new File(sourcePath + "..\\blacklist.txt");
		if (f.exists()) {
			try {
				setBlacklist(new HashSet<String>(FileUtils.readLines(f)));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void analyze() {
		// Get blacklisted files (files too messy to be parsed)
		loadBlacklist();
		// Parse every .c file in the path
		getFile(new File(sourcePath));
		// Look for function definitions on every AST
		findFunctions();
		// Look for function calls for every function definition on every AST
		findFunctionCalls();
		// Get global variable dependencies
		//analyzeGlobalVariableDependencies();
		// Look for function call dependencies in every function
		analyzeFunctionCallDependencies();
	}
	
	private void analyzeFunctionCallDependencies() {
		int i = 1;
		System.out.println("Looking for function call dependencies in functions (" + getFunctions().size() + ")");
		for (Entry<FunctionDef, Function> functionMap : getFunctions().entrySet()) {
			Function function = functionMap.getValue();
			System.out.println("Function " + i++ + "/" + getFunctions().size() + " (" + function.getName() + ")");
			// For each parameter of this function...
			int parameterOrder = 0;
			for (Id parameter : function.getParameters()) {
				FindFunctionParameterUsesVisitor findFunctionParameterUsesVisitor = new FindFunctionParameterUsesVisitor(parameter);
				function.getFunctionDef().accept(findFunctionParameterUsesVisitor);
				// Get all uses of this parameter inside function
				List<Id> functionParameterUses = findFunctionParameterUsesVisitor.getFunctionParameterUses();
				for (Id functionParameterUse : functionParameterUses) {
					// Get the function scope
					List<CompoundStatement> scope = new ArrayList<CompoundStatement>();
					for (int k = 0; k < function.getFunctionDef().getChildren().size(); k++) {
						if (function.getFunctionDef().getChildren().get(k) instanceof CompoundStatement) {
							scope.add((CompoundStatement) function.getFunctionDef().getChildren().get(k));
						}
					}
					FindVariableDeclarationVisitor findVariableDeclarationVisitor = new FindVariableDeclarationVisitor(functionParameterUse, scope, false);
					// Search for homonymous variables
					function.getFunctionDef().accept(findVariableDeclarationVisitor);
					if (!(findVariableDeclarationVisitor.isFound())) {
						// No homonymous variable found
						// For each call, check if there is a dependency
						for (FunctionCall functionCall : function.getFunctionCalls()) {
							// Find the argument
							
							// Non equivalent presence condition
							if ((!(functionCall.getPresenceCondition().equivalentTo(functionParameterUse.getPresenceCondition())) &&
								// Presence condition in function call *and* parameter use cannot be a contradiction
								(!(functionCall.getPresenceCondition().and(functionParameterUse.getPresenceCondition()).isContradiction())))) {
								Dependency dependency = new FunctionCallDependency(function, functionCall.getPresenceCondition().and(functionParameterUse.getPresenceCondition()), functionParameterUse.getPresenceCondition(), functionCall.getPresenceCondition(), functionParameterUse, functionCall);
								((FunctionCallDependency) dependency).setParameterOrder(parameterOrder);
								Call call = new Call(((FunctionCallDependency) dependency).getFunctionCall());
								if (call.getParameter(parameterOrder) == null) {
									continue;
								}
								((FunctionCallDependency) dependency).setParameter(call.getParameter(parameterOrder));
								// Discard any dependency on stubs file
								if ((dependency.getInnerFile().equals(getStubsPath())) || (dependency.getOuterFile().equals(getStubsPath()))) {
									continue;
								}
								FindFunctionDefVisitor findFunctionDefVisitor = new FindFunctionDefVisitor();
								functionCall.accept(findFunctionDefVisitor);
								Function caller = getFunctions().get(findFunctionDefVisitor.getFunctionDef());
								((FunctionCallDependency) dependency).setCaller(caller);
								function.getDependencies().add(dependency);
								getDependencies().add(dependency);
							}
						}
					} else {
						// A local variable has the same name as the parameter
						// For each call, check if there is a dependency
						for (FunctionCall functionCall : function.getFunctionCalls()) {
							// Non equivalent presence condition
							if ((!(functionCall.getPresenceCondition().equivalentTo(functionParameterUse.getPresenceCondition())) &&
								// Presence condition in function call *and* parameter use *and (not)* homonymous variable declaration cannot be a contradiction
								(!(functionCall.getPresenceCondition().and(functionParameterUse.getPresenceCondition()).and(findVariableDeclarationVisitor.getPresenceCondition()).isContradiction())))) {
									Dependency dependency = new FunctionCallDependency(function, functionCall.getPresenceCondition().and(functionParameterUse.getPresenceCondition()).and(findVariableDeclarationVisitor.getPresenceCondition()), functionParameterUse.getPresenceCondition(), functionCall.getPresenceCondition(), functionParameterUse, functionCall);
									((FunctionCallDependency) dependency).setParameterOrder(parameterOrder);
									Call call = new Call(((FunctionCallDependency) dependency).getFunctionCall());
									((FunctionCallDependency) dependency).setParameter(call.getParameter(parameterOrder));
								// Discard any dependency on stubs file
								if ((dependency.getInnerFile().equals(getStubsPath())) || (dependency.getOuterFile().equals(getStubsPath()))) {
									continue;
								}
								FindFunctionDefVisitor findFunctionDefVisitor = new FindFunctionDefVisitor();
								functionCall.accept(findFunctionDefVisitor);
								Function caller = getFunctions().get(findFunctionDefVisitor.getFunctionDef());
								((FunctionCallDependency) dependency).setCaller(caller);
								function.getDependencies().add(dependency);
								getDependencies().add(dependency);
							}
						}
					}
				}
				parameterOrder++;
			}
			System.out.println(function.getDependencies().size() + " function call dependencies found");
		}
	}

	private void analyzeGlobalVariableDependencies() {
		int i = 1;
		System.out.println("Looking for global variable dependencies calls on ASTs (" + getAsts().size() + ")");
		for (Ast ast : getAsts()) {
			System.out.println("AST " + i++ + "/" + getAsts().size());
			Node myAst = ast.getNode();
			int j = 1;
			
			FindGlobalVariablesDeclarationsVisitor findGlobalVariablesDeclarationsVisitor = new FindGlobalVariablesDeclarationsVisitor();
			myAst.accept(findGlobalVariablesDeclarationsVisitor);
			// Get all global variables on this AST
			List<Id> globalVariablesDeclarations = findGlobalVariablesDeclarationsVisitor.getGlobalVariablesDeclarations();
			for (Id globalVariableDeclaration : globalVariablesDeclarations) {
				System.out.println("AST " + i + "/" + getAsts().size() + " - Global variable " + j++ + "/" + globalVariablesDeclarations.size());
				
				FindGlobalVariableUsesVisitor findGlobalVariableUsesVisitor = new FindGlobalVariableUsesVisitor(globalVariableDeclaration);
				myAst.accept(findGlobalVariableUsesVisitor);
				// Get all uses for this global variable
				List<Id> globalVariableUses = findGlobalVariableUsesVisitor.getGlobalVariableUses();
				
				for (Id globalVariableUse : globalVariableUses) {
								
					FindVariableFunctionVisitor findVariableFunctionVisitor = new FindVariableFunctionVisitor();
					globalVariableUse.accept(findVariableFunctionVisitor);
					
					FunctionDef functionDef = findVariableFunctionVisitor.getFunctionDef();
					// If global variable use is outside a function, skip it
					if (functionDef == null) {
						continue;
					}
					
					Function function = getFunctions().get(functionDef);
					// Get the scope of the expression containing the global variable
					List<CompoundStatement> scope = findVariableFunctionVisitor.getScope();
					// Looks for a homonymous variable declared within the scope
					FindVariableDeclarationVisitor findVariableDeclarationVisitor = new FindVariableDeclarationVisitor(globalVariableUse, scope, true);
					functionDef.accept(findVariableDeclarationVisitor);
					// No homonymous variable found
					if (!(findVariableDeclarationVisitor.isFound())) {
						// Non equivalent presence condition
						if ((!(globalVariableDeclaration.getPresenceCondition().equivalentTo(globalVariableUse.getPresenceCondition()))) &&
							// Combined presence conditions are not a contradiction
							(!(globalVariableDeclaration.getPresenceCondition().and(globalVariableUse.getPresenceCondition()).isContradiction()))) {
							Dependency dependency = new GlobalVariableDependency(function, globalVariableDeclaration.getPresenceCondition().and(globalVariableUse.getPresenceCondition()), globalVariableUse.getPresenceCondition(), globalVariableDeclaration.getPresenceCondition(), globalVariableUse, globalVariableDeclaration);
							// Discard any dependency on stubs file
							if ((dependency.getInnerFile().equals(this.stubsPath)) || (dependency.getOuterFile().equals(this.stubsPath))) {
								continue;
							}
							getDependencies().add(dependency);
						}
						//functionMetricsMap.get(dependency.getFunction().getName()).getDependencies().add(dependency);
					} else {
						// A local variable has the same name as the parameter
						// Non equivalent presence condition
						if ((!(findVariableDeclarationVisitor.getPresenceCondition().equivalentTo(globalVariableUse.getPresenceCondition()))) &&
							// Combined presence conditions are not a contradiction
							(!(globalVariableDeclaration.getPresenceCondition().and(globalVariableUse.getPresenceCondition()).and(findVariableDeclarationVisitor.getPresenceCondition())).isContradiction())) {
							// Presence condition now depends on the non-declaration of the local variable 
							Dependency dependency = new GlobalVariableDependency(function, globalVariableDeclaration.getPresenceCondition().and(globalVariableUse.getPresenceCondition()).and(findVariableDeclarationVisitor.getPresenceCondition()), globalVariableUse.getPresenceCondition(), globalVariableDeclaration.getPresenceCondition(), globalVariableUse, globalVariableDeclaration);
							// Discard any dependency on stubs file
							if ((dependency.getInnerFile().equals(this.stubsPath)) || (dependency.getOuterFile().equals(this.stubsPath))) {
								continue;
							}
							getDependencies().add(dependency);
							//functionMetricsMap.get(dependency.getFunction().getName()).getDependencies().add(dependency);
						}
					}
				}
			}
		}
	}

	public void findFunctions() {
		int i = 1;
		System.out.println("Looking for function definitions on ASTs (" + getAsts().size() + ")");
		for (Ast ast : getAsts()) {
			System.out.println("AST " + i++ + "/" + getAsts().size());
			Node myAst = ast.getNode();
			FindFunctionsVisitor findFunctionsVisitor = new FindFunctionsVisitor();
			myAst.accept(findFunctionsVisitor);
			// Get all function definitions on this AST
			List<FunctionDef> functionDefs = findFunctionsVisitor.getFunctions();
			System.out.println(functionDefs.size() + " function definitions found");
			// Create a function object for each function definition
			for (FunctionDef functionDef : functionDefs) {
				FindFunctionDirectivesVisitor findFunctionDirectivesVisitor = new FindFunctionDirectivesVisitor(functionDef);
				functionDef.accept(findFunctionDirectivesVisitor);
				// Get function directives
				Set<Opt> functionDirectives = findFunctionDirectivesVisitor.getFunctionDirectives();
				// Get function inner directives (ifdefs inside function)
				Set<Opt> functionOpts = findFunctionDirectivesVisitor.getFunctionOpts();
				FindFunctionParametersVisitor findFunctionParametersVisitor = new FindFunctionParametersVisitor();
				functionDef.accept(findFunctionParametersVisitor);
				// Get function parameters
				List<Id> functionParameters = findFunctionParametersVisitor.getFunctionParameters();
				Function function = new Function(functionDef, functionParameters);
				function.setDirectives(functionDirectives);
				function.setOpts(functionOpts);
				getFunctions().put(functionDef, function);
			}
		}
	}
	
	public void findFunctionCalls() {
		int i = 1;
		System.out.println("Looking for function calls on ASTs (" + getAsts().size() + ")");
		for (Ast ast : getAsts()) {
			System.out.println("AST " + i + "/" + getAsts().size());
			Node myAst = ast.getNode();
			int j = 1;
			for (Entry<FunctionDef, Function> functionMap : getFunctions().entrySet()) {
				Function function = functionMap.getValue();
				System.out.println("AST " + i + "/" + getAsts().size() + " - Function " + j++ + "/" + getFunctions().size() + " (" + function.getName() + ")");
				FindFunctionCallsVisitor findFunctionCallsVisitor = new FindFunctionCallsVisitor(function.getFunctionDef());
				myAst.accept(findFunctionCallsVisitor);
				// Get all calls for this function
				List<FunctionCall> functionCalls = findFunctionCallsVisitor.getFunctionCalls();
				System.out.println(functionCalls.size() + " function calls found");
				function.getFunctionCalls().addAll(functionCalls);
			}
			i++;
		}
	}

	public void generateMetrics() {
		// Get every call to a function with dependency
		//measureMaintenancePoints();
		// Get every reference to a variable which causes dependency inside a function
		//measureImpactPoints();
		// Get all chained calls to a function which causes dependency
		measureDependencyDepths();
	}	
	
	private void measureImpactPoints() {
		// Acrescentar depend�ncias tanto nas fun��es caller como callee
		for (Dependency dependency : getDependencies()) {
			Function function = dependency.getCallee();
			String filename = function.getFunctionDef().getPositionFrom().getFile();
			// Reference of the variable in a dependency
			String reference = dependency.getInnerFile() + ":" + dependency.getInnerPosition();
			// A impact point contains the filename, the function and the references for variables in a dependency
			if (getImpactPoints().containsKey(filename)) {
				if (getImpactPoints().get(filename).containsKey(function.getName())) {
					getImpactPoints().get(filename).get(function.getName()).add(reference);
				} else {
					Set<String> references = new HashSet<String>();
					references.add(reference);
					getImpactPoints().get(filename).put(function.getName(), references);
				}
			} else {
				Map<String, Set<String>> functionReferences = new HashMap<String, Set<String>>();
				Set<String> references = new HashSet<String>();
				references.add(reference);
				functionReferences.put(function.getName(), references);
				getImpactPoints().put(filename, functionReferences);
			}
		}
	}

	private void measureMaintenancePoints() {
		for (Dependency dependency : getDependencies()) {
			if (!(dependency instanceof FunctionCallDependency)) {
				continue;
			}
			Function function = ((FunctionCallDependency) dependency).getCaller();
			String filename = function.getFunctionDef().getPositionFrom().getFile();
			// Entry point a function call dependency
			String entryPoint = dependency.getOuterFile() + ":" + dependency.getOuterPosition();
			// A maintenance point contains the filename, the function and its entry points
			if (getMaintenancePoints().containsKey(filename)) {
				if (getMaintenancePoints().get(filename).containsKey(function.getName())) {
					getMaintenancePoints().get(filename).get(function.getName()).add(entryPoint);
				} else {
					Set<String> entryPoints = new HashSet<String>();
					entryPoints.add(entryPoint);
					getMaintenancePoints().get(filename).put(function.getName(), entryPoints);
				}
			} else {
				Map<String, Set<String>> functionEntryPoints = new HashMap<String, Set<String>>();
				Set<String> entryPoints = new HashSet<String>();
				entryPoints.add(entryPoint);
				functionEntryPoints.put(function.getName(), entryPoints);
				getMaintenancePoints().put(filename, functionEntryPoints);
			}
			
		}
	}
	
	private void measureDependencyDepths() {
		System.out.println("Checking dependencies depth (" + dependencies.size() + ")");
		int i = 1;
		int depth = 1;
		List<Chain> chains = new ArrayList<Chain>();
		System.out.println("Checking depth: " + depth);
		for (Dependency dependency : dependencies) {
			System.out.println("Dependency " + i++ + "/" + dependencies.size());
			
			if (dependency instanceof GlobalVariableDependency) {
				// Global variable dependencies do not "chain"
				continue;
			}
			
			Function calleeFunction = dependency.getCallee();
			Function callerFunction = ((FunctionCallDependency) dependency).getCaller();
			List<Function> callers = new ArrayList<Function>();
			callers.add(callerFunction);
		
			// Parameter position indicates which function argument links caller to callee
			int parameterPosition = 0;
			// Check all parameters of caller function to check if any is passed to the callee as the dependent argument
			for (Id parameter : callerFunction.getParameters()) {
				FindFunctionParameterUsesVisitor findFunctionParameterUsesVisitor = new FindFunctionParameterUsesVisitor(parameter);
				// If no parameter found at position...
				if (((FunctionCallDependency) dependency).getParameter() == null) {
					continue;
				}
				((FunctionCallDependency) dependency).getParameter().accept(findFunctionParameterUsesVisitor);
				
				if (!(findFunctionParameterUsesVisitor.getFunctionParameterUses().isEmpty())) {
					// All direct dependencies have a depth of 1
					Chain chain = new Chain(calleeFunction, callers, depth, parameterPosition);
					chains.add(chain);
				}
				parameterPosition++;
			}
		}
		getChainedFunctions().addAll(chains);
		getNextDepthLevel(chains);
	}
	
	public void getNextDepthLevel(List<Chain> chains) {
		List<Chain> newChains = new ArrayList<Chain>();
		for (Chain chain : chains) {
			// Get all functions which calls chain caller
			List<Function> callers = chain.getCallers();
			Function callee = chain.getCallee();
			Function lastCaller = callers.get(callers.size() - 1);
			// main() will not be called by another function
			if (lastCaller.getName().equals("main")) {
				continue;
			}
			//List<FunctionCall> functionCalls = new ArrayList<FunctionCall>();
			for (Ast ast : getAsts()) {
				Node myAst = ast.getNode();
				FindFunctionCallsVisitor findFunctionCallsVisitor = new FindFunctionCallsVisitor(lastCaller.getFunctionDef());
				myAst.accept(findFunctionCallsVisitor);
				List<FunctionCall> functionCalls = findFunctionCallsVisitor.getFunctionCalls();			
				// Check if xth argument of each call comes from caller's arguments
				for (FunctionCall functionCall : functionCalls) {
					FindFunctionDefVisitor findFunctionDefVisitor = new FindFunctionDefVisitor();
					functionCall.accept(findFunctionDefVisitor);
					FunctionDef functionDef = findFunctionDefVisitor.getFunctionDef();
					Function caller = getFunctions().get(functionDef);
					FindFunctionCallVariablesVisitor findFunctionCallVariablesVisitor = new FindFunctionCallVariablesVisitor(chain.getParameterPosition());
					functionCall.accept(findFunctionCallVariablesVisitor);
					List<Id> functionCallVariables = findFunctionCallVariablesVisitor.getVariables();
					System.out.println(functionCallVariables.size() + " variables found at " + chain.getParameterPosition() + "th position of call to " + caller.getName());
					int i = 0;
					for (Id parameter : caller.getParameters()) {
						for (Id functionCallVariable : functionCallVariables) {
							if (functionCallVariable.getName().equals(parameter.getName())) {
								List<Function> newCallers = new ArrayList<Function>(callers);
								if (callers.contains(caller)) {
									System.out.println("Function already referenced. Ignoring...");
								} else {
									newCallers.add(caller);
									Chain newChain = new Chain(callee, newCallers, chain.getDepth()+1, i);
									newChains.add(newChain);
									System.out.println("New chain found! Depth: " + (chain.getDepth()+1) + ", callers: " + newCallers.size());
								}
								// Try to create a list of functions for caller, so it'll be easy to avoid ciclic references
							}
						}
						i++;
					}
				}
			}
		}
		if (!(newChains.isEmpty())) {
			getChainedFunctions().addAll(newChains);
			getNextDepthLevel(newChains);
		}
	}
}
