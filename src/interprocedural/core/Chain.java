package interprocedural.core;

import java.util.ArrayList;
import java.util.List;

public class Chain {
	private Function callee;
	private List<Function> callers = new ArrayList<Function>();
	int depth;
	int parameterPosition;
	
	public Chain(Function callee, List<Function> callers, int depth, int parameterPosition) {
		this.callee = callee;
		this.callers = callers;
		this.depth = depth;
		this.parameterPosition = parameterPosition;
	}

	public Function getCallee() {
		return callee;
	}

	public void setCallee(Function callee) {
		this.callee = callee;
	}

	public List<Function> getCallers() {
		return callers;
	}

	public void setCallers(List<Function> callers) {
		this.callers = callers;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public int getParameterPosition() {
		return parameterPosition;
	}

	public void setParameterPosition(int parameterPosition) {
		this.parameterPosition = parameterPosition;
	}
	
}
