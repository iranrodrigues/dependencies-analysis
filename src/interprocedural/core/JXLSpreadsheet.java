package interprocedural.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import tree.FunctionDef;

public class JXLSpreadsheet {
	
	WritableCellFormat floatPercentage = new WritableCellFormat(NumberFormats.PERCENT_FLOAT);
	Project project;
	WritableWorkbook workbook;

	public JXLSpreadsheet(Project project) {
		this.project = project;
	}

	public WritableCellFormat getFloatPercentage() {
		return floatPercentage;
	}

	public void setFloatPercentage(WritableCellFormat floatPercentage) {
		this.floatPercentage = floatPercentage;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public WritableWorkbook getWorkbook() {
		return workbook;
	}

	public void setWorkbook(WritableWorkbook workbook) {
		this.workbook = workbook;
	}

	public void createSpreadsheet(String fileName) {
		try {
			// Create file
			workbook = Workbook.createWorkbook(new File(fileName));			
			createSheetFunctions();
			//createSheetDepths(workbook, dependencyDepthsMap);
			createSheetAllDependencies();
			//createSheetPossibleDependencies(workbook, possibleDependenciesMap);
			//createSheetProbableDependencies(workbook, probableDependenciesMap);
			//createSheetSummary(workbook, functionMetricsMapMap, dependenciesMap, possibleDependenciesMap, analyzedFileCount);
			//createSheetUniqueDependencies(workbook, dependenciesMap);
			//createSheetUniqueDependenciesSummary(workbook, dependenciesMap);
			
			workbook.write();
			workbook.close();
			System.out.println("Spreadsheet created.");
		} catch (FileNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		}
	}
	
	public void createSheetFunctions() {
		WritableSheet sheet = getWorkbook().createSheet("Functions", getWorkbook().getNumberOfSheets());

		// Inserting header
		String[] headers = {"FILE", "FUNCTION", "PARAMETERS", "DEPENDENCIES", "CALLS", "HAS_DIRECTIVES"};
		for (int i = 0; i < headers.length; i++) {
			try {
				sheet.addCell(new Label(i, 0, headers[i]));
			} catch (RowsExceededException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (WriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		int i = 1;
		try {
			for (Entry<FunctionDef, Function> functionMap : getProject().getFunctions().entrySet()) {
				FunctionDef functionDef = functionMap.getKey();
				Function function = functionMap.getValue();
				// Filename
				sheet.addCell(new Label(0, i, functionDef.getPositionFrom().getFile().substring(5)));
				// Function name
				sheet.addCell(new Label(1, i, function.getName()));
				// Total of parameters
				sheet.addCell(new Number(2, i, function.getParameters().size()));
				// Total of dependencies
				sheet.addCell(new Number(3, i, function.getDependencies().size()));
				// Total of calls
				sheet.addCell(new Number(4, i, function.getFunctionCalls().size()));
				// Has directives?
				sheet.addCell(new Label(5, i, new Boolean(function.getDirectives().size() > 0).toString()));
				i++;
			}
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createSheetDepths(WritableWorkbook workbook, Map<String, Map<String, Map<String, Integer>>> dependencyDepthsMap) {
		WritableSheet sheet = workbook.createSheet("Dependencies depths", 1);

		// Inserting header
		String[] headers = {"FILE", "FUNCTION_WITH_DEPENDENCY", "TRIGGER_FUNCTION", "DEPTH"};
		for (int i = 0; i < headers.length; i++) {
			try {
				sheet.addCell(new Label(i, 0, headers[i]));
			} catch (RowsExceededException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (WriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		int i = 1;
		try {
			for (Entry<String, Map<String, Map<String, Integer>>> fileDependencyDepths : dependencyDepthsMap.entrySet()) {
				for (Entry<String, Map<String, Integer>> functionDependencyDepths : fileDependencyDepths.getValue().entrySet()) {
					for (Entry<String, Integer> dependencyDepth : functionDependencyDepths.getValue().entrySet()) {
						//System.out.println(fileDependencyDepths.getKey() + " - " + functionDependencyDepths.getKey() + " - " + dependencyDepth.getKey() + " = " + dependencyDepth.getValue());
						sheet.addCell(new Label(0, i, fileDependencyDepths.getKey()));
						sheet.addCell(new Label(1, i, functionDependencyDepths.getKey()));
						sheet.addCell(new Label(2, i, dependencyDepth.getKey()));
						sheet.addCell(new Number(3, i, dependencyDepth.getValue()));
						i++;
					}
				}
			}
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createSheetAllDependencies() {
		WritableSheet sheet = getWorkbook().createSheet("Dependencies", getWorkbook().getNumberOfSheets());

		// Inserting header
		String[] headers = {"DIRECTION", "TYPE", "PRESENCE_CONDITION", "OUTER_PRESENCE_CONDITION", "INNER_PRESENCE_CONDITION", "FUNCTION", "VARIABLE", "IS_ASSIGNMENT", "OUTER_FILENAME", "OUTER_POSITION", "INNER_FILENAME", "INNER_POSITION"};
		for (int i = 0; i < headers.length; i++) {
			try {
				sheet.addCell(new Label(i, 0, headers[i]));
			} catch (RowsExceededException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (WriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		int i = 1;
		try {
			for (Dependency dependency : getProject().getDependencies()) {
				// toString() returns all dependency metrics
				String[] data = dependency.toString().split(";");
				for (int j = 0; j < data.length; j++) {
					if (data[j].length() > 32766) {
						sheet.addCell(new Label(j, i, data[j].substring(0,32766)));
					} else {
						sheet.addCell(new Label(j, i, data[j]));
					}
				}
				i++;
			}
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}