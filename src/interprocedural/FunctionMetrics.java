package interprocedural;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FunctionMetrics {
	private Function function;
	private List<Dependency> dependencies = new ArrayList<Dependency>();
	private List<PossibleDependency> possibleDependencies = new ArrayList<PossibleDependency>();
	
	public FunctionMetrics(Function function) {
		this.function = function;
	}

	public Function getFunction() {
		return function;
	}
	
	public List<Dependency> getDependencies() {
		return dependencies;
	}
	
	public List<PossibleDependency> getPossibleDependencies() {
		return possibleDependencies;
	}
	
	public Set<String> getDependentParameters() {
		Set<String> dependentParameters = new HashSet<String>();
		for (Dependency dependency : dependencies) {
			if (dependency instanceof FunctionCallDependency) {
				dependentParameters.add(dependency.getVariable().getName());
			}
		}
		return dependentParameters;
	}
	
	public Set<String> getPossibleDependentParameters() {
		Set<String> possibleDependentParameters = new HashSet<String>();
		for (PossibleDependency possibleDependency : possibleDependencies) {
			possibleDependentParameters.add(possibleDependency.getVariable().getName());
		}
		return possibleDependentParameters;
	}
	
	public Set<String> getDependentGlobalVariables() {
		Set<String> dependentGlobalVariables = new HashSet<String>();
		for (Dependency dependency : dependencies) {
			if (dependency instanceof GlobalVariableDependency) {
				dependentGlobalVariables.add(dependency.getVariable().getName());
			}
		}
		return dependentGlobalVariables;
	}
	
	public String toString() {
		return 
			getFunction().getName() + ";" +
			getFunction().getParameters().size() + ";" +
			getDependentParameters().size() + ";" +
			getPossibleDependentParameters().size() + ";" +
			getDependentGlobalVariables().size() + ";" +
			getDependencies().size() + ";" +
			getPossibleDependencies().size() + ";" +
			getFunction().getFunctionCalls().size() + ";" +
			(getFunction().getDirectives().size() > 0);
	}
	
}
