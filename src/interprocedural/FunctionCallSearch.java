package interprocedural;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import tree.FunctionCall;
import tree.FunctionDef;
import tree.Node;
import tree.TranslationUnit;
import tree.visitor.VisitorASTOrganizer;
import core.ASTGenerator;
import de.fosd.typechef.FrontendOptions;
import de.fosd.typechef.FrontendOptionsWithConfigFiles;
import de.fosd.typechef.Lex;
import de.fosd.typechef.lexer.options.OptionException;
import de.fosd.typechef.parser.TokenReader;
import de.fosd.typechef.parser.c.AST;
import de.fosd.typechef.parser.c.CParser;
import de.fosd.typechef.parser.c.CToken;
import de.fosd.typechef.parser.c.CTypeContext;
import de.fosd.typechef.parser.c.ParserMain;

public class FunctionCallSearch {

	private String stubsPath;
	private List<FunctionCall> functionCalls = new ArrayList<FunctionCall>();
	private List<PossibleDependency> possibleDependencies = new ArrayList<PossibleDependency>();
	
	public String getStubsPath() {
		return stubsPath;
	}

	public List<FunctionCall> getFunctionCalls() {
		return functionCalls;
	}
	
	public FunctionCallSearch() {
		
	}

	public void doSearch(String filePath, String stubsPath, Map<String, List<PossibleDependency>> possibleDependenciesMap) {
		this.stubsPath = stubsPath;
		List<PossibleDependency> possibleDependencies = new ArrayList<PossibleDependency>();
		for (Entry<String, List<PossibleDependency>> possibleDependenciesEntry : possibleDependenciesMap.entrySet()) {
			possibleDependencies.addAll(possibleDependenciesEntry.getValue());
		}
		this.possibleDependencies = possibleDependencies;
		this.getFiles(new File(filePath));
	}

	public void getFiles(File path) {
		if (path.isDirectory()) {
			for (File file : path.listFiles()) {
				this.getFiles(file);
			}
		} else {
			if (!path.getName().startsWith(".")) {
				if (path.getName().endsWith(".c")) {
						//|| path.getName().endsWith(".h")) {
					System.out.println("FILE: " + path.getAbsolutePath());
					this.searchFile(path);
				}
			}
		}
	}

	public void searchFile(File file) {
		FrontendOptions myParserOptions = new FrontendOptionsWithConfigFiles();
		ArrayList<String> parameters = new ArrayList<String>();

		parameters.add("--lexNoStdout");
		parameters.add("-h");
		parameters.add(this.stubsPath);
		parameters.add(file.getPath());

		String[] parameterArray = parameters
				.toArray(new String[parameters.size()]);

		try {
			myParserOptions.parseOptions(parameterArray);
		} catch (OptionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ParserMain parser = new ParserMain(new CParser(null, false));

		TokenReader<CToken, CTypeContext> in = Lex.lex(myParserOptions);

		AST ast = parser.parserMain(in, myParserOptions);
		Node myAst = new TranslationUnit();
		ASTGenerator astGenerator =	new ASTGenerator();
		astGenerator.generate(ast, myAst);
		VisitorASTOrganizer visitorASTOrganizer = new VisitorASTOrganizer();
		myAst.accept(visitorASTOrganizer);
		/*
		List<Id> functions = new ArrayList<Id>();
		for (PossibleDependency possibleDependency : this.possibleDependencies) {
			
			FindAllFunctionCallsVisitor findAllFunctionCallsVisitor = new FindAllFunctionCallsVisitor(functions);
			myAst.accept(findFunctionCallsVisitor);
			possibleDependency.getFunction().setFunctionCalls(findFunctionCallsVisitor.getFunctionCalls());
			System.out.println("Calls for " + possibleDependency.getFunction().getName());
			for (FunctionCall fc : findFunctionCallsVisitor.getFunctionCalls()) {
				System.out.println(fc.getPositionFrom());
			}
		}*/		
		
	}

}
