package interprocedural;

import tree.Id;
import de.fosd.typechef.featureexpr.FeatureExpr;

public class PossibleDependency extends Dependency {

	public PossibleDependency(Function function, FeatureExpr presenceCondition,
			FeatureExpr innerPresenceCondition, Id variable) {
		super(function, presenceCondition, innerPresenceCondition, null, variable);
	}

	@Override
	public String getOuterFile() {
		return null;
	}

	@Override
	public String getOuterPosition() {
		return null;
	}
	
	public String toString() {
		return
			getClass().getSimpleName() + ";" +
			getInnerPresenceCondition().toString() + ";" +
			getFunction().getName() + ";" +
			getVariable().getName() + ";" +
			isLeftSideAssignment() + ";" +
			getInnerFile() + ";" +
			getInnerPosition();
	}

}
