package interprocedural;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class Spreadsheet {
	
	WritableCellFormat floatPercentage = new WritableCellFormat(NumberFormats.PERCENT_FLOAT);
	ProjectAnalyzer projectAnalyzer;

	public Spreadsheet(ProjectAnalyzer projectAnalyzer) {
		this.projectAnalyzer = projectAnalyzer;
	}

	public void createSpreadsheet(String fileName) {
		try {
			Map<String, Map<String, FunctionMetrics>> functionMetricsMapMap = this.projectAnalyzer.getFunctionMetricsMap();
			Map<String, Map<String, Map<String, Integer>>> dependencyDepthsMap = this.projectAnalyzer.getDependencyDepthsMap();
			Map<String, List<Dependency>> dependenciesMap = this.projectAnalyzer.getDependenciesMap();
			Map<String, List<PossibleDependency>> possibleDependenciesMap = this.projectAnalyzer.getPossibleDependenciesMap();
			Map<String, List<Dependency>> probableDependenciesMap = this.projectAnalyzer.getProbableDependenciesMap();
			int analyzedFileCount = this.projectAnalyzer.getAnalyzedFileCount(); 
			
			// Create file
			WritableWorkbook workbook = Workbook.createWorkbook(new File(fileName));			
			createSheetFunctions(workbook, functionMetricsMapMap);
			createSheetDepths(workbook, dependencyDepthsMap);
			createSheetAllDependencies(workbook, dependenciesMap);
			createSheetPossibleDependencies(workbook, possibleDependenciesMap);
			createSheetProbableDependencies(workbook, probableDependenciesMap);
			createSheetSummary(workbook, functionMetricsMapMap, dependenciesMap, possibleDependenciesMap, analyzedFileCount);
			//createSheetUniqueDependencies(workbook, dependenciesMap);
			//createSheetUniqueDependenciesSummary(workbook, dependenciesMap);
			
			workbook.write();
			workbook.close();
			System.out.println("Spreadsheet created.");
		} catch (FileNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		}
	}
	
	public void createSheetFunctions(WritableWorkbook workbook, Map<String, Map<String, FunctionMetrics>> functionMetricsMapMap) {
		WritableSheet sheet = workbook.createSheet("Functions", 0);

		// Inserting header
		String[] headers = {"FILE", "FUNCTION", "PARAMETERS", "DEPENDENT_PARAMETERS", "POSSIBLE_DEPENDENT_PARAMETERS", "DEPENDENT_GLOBAL_VARIABLES", "DEPENDENCIES", "POSSIBLE_DEPENDENCIES", "CALLS", "%_OF_PARAMS_WITH_DEPENDENCY", "HAS_DIRECTIVES"};
		for (int i = 0; i < headers.length; i++) {
			try {
				sheet.addCell(new Label(i, 0, headers[i]));
			} catch (RowsExceededException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (WriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		int i = 1;
		try {
			for (Entry<String, Map<String, FunctionMetrics>> functionMetricsMap : functionMetricsMapMap.entrySet()) {
				for (Entry<String, FunctionMetrics> functionMetrics : functionMetricsMap.getValue().entrySet()) {
					String[] data = (functionMetricsMap.getKey() + ";" + functionMetrics.getValue()).split(";");
					for (int j = 0; j < data.length; j++) {
						if (j < 2) {
							sheet.addCell(new Label(j, i, data[j]));
						} else if (j == 9) { // HAS_DIRECTIVES?
							sheet.addCell(new Label(10, i, data[j]));
						} else {
							sheet.addCell(new Number(j, i, Integer.parseInt(data[j])));
						}
					}
					// Dependent parameters / parameters
					//sheet.addCell(new Formula(7, i, "IF(C" + (i + 1) + "=0,0,D" + (i + 1) + "/C" + (i + 1) + ")*100"));
					if (functionMetrics.getValue().getFunction().getParameters().size() != 0) {
						sheet.addCell(new Number(9, i, (double)functionMetrics.getValue().getDependentParameters().size()/(double)functionMetrics.getValue().getFunction().getParameters().size(), this.floatPercentage));
					} else {
						sheet.addCell(new Number(9, i, 0, this.floatPercentage));
					}
					i++;
				}
			}
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createSheetDepths(WritableWorkbook workbook, Map<String, Map<String, Map<String, Integer>>> dependencyDepthsMap) {
		WritableSheet sheet = workbook.createSheet("Dependencies depths", 1);

		// Inserting header
		String[] headers = {"FILE", "FUNCTION_WITH_DEPENDENCY", "TRIGGER_FUNCTION", "DEPTH"};
		for (int i = 0; i < headers.length; i++) {
			try {
				sheet.addCell(new Label(i, 0, headers[i]));
			} catch (RowsExceededException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (WriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		int i = 1;
		try {
			for (Entry<String, Map<String, Map<String, Integer>>> fileDependencyDepths : dependencyDepthsMap.entrySet()) {
				for (Entry<String, Map<String, Integer>> functionDependencyDepths : fileDependencyDepths.getValue().entrySet()) {
					for (Entry<String, Integer> dependencyDepth : functionDependencyDepths.getValue().entrySet()) {
						//System.out.println(fileDependencyDepths.getKey() + " - " + functionDependencyDepths.getKey() + " - " + dependencyDepth.getKey() + " = " + dependencyDepth.getValue());
						sheet.addCell(new Label(0, i, fileDependencyDepths.getKey()));
						sheet.addCell(new Label(1, i, functionDependencyDepths.getKey()));
						sheet.addCell(new Label(2, i, dependencyDepth.getKey()));
						sheet.addCell(new Number(3, i, dependencyDepth.getValue()));
						i++;
					}
				}
			}
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createSheetAllDependencies(WritableWorkbook workbook, Map<String, List<Dependency>> dependenciesMap) {
		WritableSheet sheet = workbook.createSheet("Dependencies", 2);

		// Inserting header
		String[] headers = {"FILE", "DIRECTION", "TYPE", "PRESENCE_CONDITION", "OUTER_PRESENCE_CONDITION", "INNER_PRESENCE_CONDITION", "FUNCTION", "VARIABLE", "IS_ASSIGNMENT", "OUTER_FILENAME", "OUTER_POSITION", "INNER_FILENAME", "INNER_POSITION"};
		for (int i = 0; i < headers.length; i++) {
			try {
				sheet.addCell(new Label(i, 0, headers[i]));
			} catch (RowsExceededException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (WriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		int i = 1;
		try {
			for (Entry<String, List<Dependency>> dependenciesList : dependenciesMap.entrySet()) {
				for (Dependency dependency : dependenciesList.getValue()) {
					String[] data = (dependenciesList.getKey() + ";" + dependency).split(";");
					for (int j = 0; j < data.length; j++) {
						if (data[j].length() > 32766) {
							sheet.addCell(new Label(j, i, data[j].substring(0,32766)));
						} else {
							sheet.addCell(new Label(j, i, data[j]));
						}
					}
					i++;
				}
			}
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createSheetPossibleDependencies(WritableWorkbook workbook, Map<String, List<PossibleDependency>> possibleDependenciesMap) {
		WritableSheet sheet = workbook.createSheet("Possible Dependencies", 3);

		// Inserting header
		String[] headers = {"FILE", "TYPE", "INNER_PRESENCE_CONDITION", "FUNCTION", "VARIABLE", "IS_ASSIGNMENT", "INNER_FILENAME", "INNER_POSITION"};
		for (int i = 0; i < headers.length; i++) {
			try {
				sheet.addCell(new Label(i, 0, headers[i]));
			} catch (RowsExceededException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (WriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		int i = 1;
		try {
			for (Entry<String, List<PossibleDependency>> possibleDependenciesList : possibleDependenciesMap.entrySet()) {
				for (PossibleDependency possibleDependency : possibleDependenciesList.getValue()) {
					String[] data = (possibleDependenciesList.getKey() + ";" + possibleDependency).split(";");
					for (int j = 0; j < data.length; j++) {
						if (data[j].length() > 32766) {
							sheet.addCell(new Label(j, i, data[j].substring(0,32766)));
						} else {
							sheet.addCell(new Label(j, i, data[j]));
						}
					}
					i++;
					if (i > 65535) {
						break;
					}
				}
			}
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createSheetProbableDependencies(WritableWorkbook workbook, Map<String, List<Dependency>> dependenciesMap) {
		WritableSheet sheet = workbook.createSheet("Probable Dependencies", 4);

		// Inserting header
		String[] headers = {"FILE", "DIRECTION", "TYPE", "PRESENCE_CONDITION", "OUTER_PRESENCE_CONDITION", "INNER_PRESENCE_CONDITION", "FUNCTION", "VARIABLE", "IS_ASSIGNMENT", "OUTER_FILENAME", "OUTER_POSITION", "INNER_FILENAME", "INNER_POSITION"};
		for (int i = 0; i < headers.length; i++) {
			try {
				sheet.addCell(new Label(i, 0, headers[i]));
			} catch (RowsExceededException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (WriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		int i = 1;
		try {
			for (Entry<String, List<Dependency>> dependenciesList : dependenciesMap.entrySet()) {
				for (Dependency dependency : dependenciesList.getValue()) {
					String[] data = (dependenciesList.getKey() + ";" + dependency).split(";");
					for (int j = 0; j < data.length; j++) {
						if (data[j].length() > 32766) {
							sheet.addCell(new Label(j, i, data[j].substring(0,32766)));
						} else {
							sheet.addCell(new Label(j, i, data[j]));
						}
					}
					i++;
				}
			}
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createSheetSummary(WritableWorkbook workbook, Map<String, Map<String, FunctionMetrics>> functionMetricsMapMap, Map<String, List<Dependency>> dependenciesMap, Map<String, List<PossibleDependency>> possibleDependenciesMap, int analyzedFiles) {
		WritableSheet sheet = workbook.createSheet("Summary", 5);
		int line = 0;
		// Inserting header
		String[] headers = {"Total of dependencies:", "Total of possible dependencies:", "", "Total of files:",
				"Total of analyzed files:", "% of analyzed files:","",
				"Total of analyzed files with dependencies:", "% of analyzed files with dependencies:",
				"Average dependencies per file:",
				"Average dependencies per file which has dependencies:",
				"Maximum dependency count on a single file:", "",
				"Total of analyzed files with possible dependencies:", "% of analyzed files with possible dependencies:",
				"Average possible dependencies per file:",
				"Average possible dependencies per file which has possible dependencies:",
				"Maximum possible dependency count on a single file:", "",
				"Total of functions:", "Total of functions with directives:", "% of functions with directives:", "",
				"Total of functions with dependencies:",
				"% of functions with dependencies:", "% of functions with directives which has dependencies:",
				"Average dependencies per function:",
				"Average dependencies per function which has dependencies:",
				"Maximum dependency count on a single function:", "",
				"Total of functions with possible dependencies:",
				"% of functions with possible dependencies:", "% of functions with directives which has possible dependencies:",
				"Average possible dependencies per function:",
				"Average possible dependencies per function which has possible dependencies:",
				"Maximum possible dependency count on a single function:", "",
				"Total of functions called:",
				"% of functions called:", "Total of function calls:",
				"Total of function calls for functions with dependencies:", "Average calls per function:",
				"Average calls per function which has dependencies:", "",
				"Total of function parameters:", "",
				"Total of function parameters for functions with dependencies:",
				"Total of dependent function parameters:",
				"Average dependent parameters per function:",
				"Average dependent parameters per function which has dependencies:",
				"Average dependent parameters per function which has dependent parameters:", "",
				"Total of function parameters for functions with possible dependencies:",
				"Total of possible dependent function parameters:",
				"Average possible dependent parameters per function:",
				"Average possible dependent parameters per function which has possible dependencies:",
				"Average possible dependent parameters per function which has possible dependent parameters:", "",
				"Dependencies by direction", "Mandatory->Optional:", "Optional->Mandatory:",
				"Optional->Optional:",	"", "Dependencies by type", "Global Variable Dependency:",
				"Function Call Dependency:", "",
				"Dependencies by Use/assignment", "Assignment", "Use", "",
				"Possible dependencies by Use/assignment", "Assignment", "Use"};
		
		int dependencyCount = 0;
		int maxDependencyCountSingleFile = 0;
		int filesWithDependency = 0;
		String maxDependencyCountSingleFileName = null;
		int dependencyDirectionMOCount = 0;
		int dependencyDirectionOMCount = 0;
		int dependencyDirectionOOCount = 0;
		int dependencyTypeGlobalCount = 0;
		int dependencyTypeFunctionCount = 0;
		int dependencyLSACount = 0;
		
		for (Entry<String, List<Dependency>> dependenciesList : dependenciesMap.entrySet()) {
			int dependencyCountSingleFile = 0;
			for (Dependency dependency : dependenciesList.getValue()) {
				dependencyCount++;
				if (dependencyCountSingleFile == 0) {
					filesWithDependency++;
				}
				dependencyCountSingleFile++;
				switch (dependency.getDirection()) {
				case "Mandatory<->Optional":
					dependencyDirectionMOCount++;
					break;
				case "Optional<->Mandatory":
					dependencyDirectionOMCount++;
					break;
				case "Optional<->Optional":
					dependencyDirectionOOCount++;
					break;
				}
				if (dependency instanceof GlobalVariableDependency) {
					dependencyTypeGlobalCount++;
				} else if (dependency instanceof FunctionCallDependency) {
					dependencyTypeFunctionCount++;
				}
				if (dependency.isLeftSideAssignment()) {
					dependencyLSACount++;
				}
			}
			if (dependencyCountSingleFile > maxDependencyCountSingleFile) {
				maxDependencyCountSingleFile = dependencyCountSingleFile;
				maxDependencyCountSingleFileName = dependenciesList.getKey();
			}
		}
		
		int possibleDependencyCount = 0;
		int maxPossibleDependencyCountSingleFile = 0;
		int filesWithPossibleDependency = 0;
		String maxPossibleDependencyCountSingleFileName = null;
		int possibleDependencyLSACount = 0;
		
		for (Entry<String, List<PossibleDependency>> possibleDependenciesList : possibleDependenciesMap.entrySet()) {
			int possibleDependencyCountSingleFile = 0;
			for (PossibleDependency possibleDependency : possibleDependenciesList.getValue()) {
				possibleDependencyCount++;
				if (possibleDependencyCountSingleFile == 0) {
					filesWithPossibleDependency++;
				}
				possibleDependencyCountSingleFile++;
				if (possibleDependency.isLeftSideAssignment()) {
					possibleDependencyLSACount++;
				}
			}
			if (possibleDependencyCountSingleFile > maxPossibleDependencyCountSingleFile) {
				maxPossibleDependencyCountSingleFile = possibleDependencyCountSingleFile;
				maxPossibleDependencyCountSingleFileName = possibleDependenciesList.getKey();
			}
		}
		
		int functionCount = 0;
		int functionWithDirectiveCount = 0;
		int functionWithDependencyCount = 0;
		int functionWithPossibleDependencyCount = 0;
		int maxDependencyCountSingleFunction = 0;
		String maxDependencyCountSingleFunctionName = null;
		String maxDependencyCountSingleFunctionFileName = null;
		int maxPossibleDependencyCountSingleFunction = 0;
		String maxPossibleDependencyCountSingleFunctionName = null;
		String maxPossibleDependencyCountSingleFunctionFileName = null;
		int functionCallCount = 0;
		int functionWithDependencyCallCount = 0;
		int parameterCount = 0;
		int parameterOfFunctionWithDependencyCount = 0;
		int dependentParameterCount = 0;
		int functionWithDependentParameterCount = 0;
		int parameterOfFunctionWithPossibleDependencyCount = 0;
		int possibleDependentParameterCount = 0;
		int functionWithPossibleDependentParameterCount = 0;
		int functionCalledCount = 0;
		
		for (Entry<String, Map<String, FunctionMetrics>> functionMetricsMap : functionMetricsMapMap.entrySet()) {
			for (Entry<String, FunctionMetrics> functionMetrics : functionMetricsMap.getValue().entrySet()) {
				int dependencyCountSingleFunction = 0;
				int possibleDependencyCountSingleFunction = 0;
				functionCount++;
				functionCallCount += functionMetrics.getValue().getFunction().getFunctionCalls().size();
				parameterCount += functionMetrics.getValue().getFunction().getParameters().size();
				if (functionMetrics.getValue().getFunction().getDirectives().size() > 0) {
					functionWithDirectiveCount++;
				}
				if (functionMetrics.getValue().getDependencies().size() > 0) {
					functionWithDependencyCount++;
					dependencyCountSingleFunction += functionMetrics.getValue().getDependencies().size();
					functionWithDependencyCallCount += functionMetrics.getValue().getFunction().getFunctionCalls().size();
					parameterOfFunctionWithDependencyCount += functionMetrics.getValue().getFunction().getParameters().size();
					dependentParameterCount += functionMetrics.getValue().getDependentParameters().size();
					if (functionMetrics.getValue().getDependentParameters().size() > 0) {
						functionWithDependentParameterCount++;
					}
				}
				if (functionMetrics.getValue().getPossibleDependencies().size() > 0) {
					functionWithPossibleDependencyCount++;
					possibleDependencyCountSingleFunction += functionMetrics.getValue().getPossibleDependencies().size();
					parameterOfFunctionWithPossibleDependencyCount += functionMetrics.getValue().getFunction().getParameters().size();
					possibleDependentParameterCount += functionMetrics.getValue().getPossibleDependentParameters().size();
					if (functionMetrics.getValue().getPossibleDependentParameters().size() > 0) {
						functionWithPossibleDependentParameterCount++;
					}
				}
				if (functionMetrics.getValue().getFunction().getFunctionCalls().size() > 0) {
					functionCalledCount++;
				}
				if (dependencyCountSingleFunction > maxDependencyCountSingleFunction) {
					maxDependencyCountSingleFunction = dependencyCountSingleFunction;
					maxDependencyCountSingleFunctionName = functionMetrics.getKey();
					maxDependencyCountSingleFunctionFileName = functionMetricsMap.getKey();
				}
				if (possibleDependencyCountSingleFunction > maxPossibleDependencyCountSingleFunction) {
					maxPossibleDependencyCountSingleFunction = possibleDependencyCountSingleFunction;
					maxPossibleDependencyCountSingleFunctionName = functionMetrics.getKey();
					maxPossibleDependencyCountSingleFunctionFileName = functionMetricsMap.getKey();
				}
			}
		}
		
		for (int i = 0; i < headers.length; i++) {
			try {
				sheet.addCell(new Label(0, i, headers[i]));
			} catch (RowsExceededException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (WriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			// Total of dependencies
			sheet.addCell(new Number(1, line++, dependencyCount));
			// Total of possible dependencies
			sheet.addCell(new Number(1, line++, possibleDependencyCount));
			// Total of files
			line++;
			sheet.addCell(new Number(1, line++, analyzedFiles));
			// Total of analyzed files
			sheet.addCell(new Number(1, line++, dependenciesMap.size()));
			// % of analyzed files
			if (analyzedFiles != 0) { 
				sheet.addCell(new Number(1, line++, (double)dependenciesMap.size()/(double)analyzedFiles, this.floatPercentage));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			line++;
			// Total of analyzed files with dependencies
			sheet.addCell(new Number(1, line++, filesWithDependency));
			// % of analyzed files with dependencies
			if (dependenciesMap.size() != 0) {
				sheet.addCell(new Number(1, line++, (double)filesWithDependency/(double)dependenciesMap.size(), this.floatPercentage));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Average dependencies per file
			if (dependenciesMap.size() != 0) { 
				sheet.addCell(new Number(1, line++, (double)dependencyCount/(double)dependenciesMap.size()));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Average dependencies per file which has dependencies
			if (filesWithDependency != 0) { 
				sheet.addCell(new Number(1, line++, (double)dependencyCount/(double)filesWithDependency));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Max dependency count on a single file
			sheet.addCell(new Number(1, line, maxDependencyCountSingleFile));
			sheet.addCell(new Label(2, line, "File:"));
			sheet.addCell(new Label(3, line++, maxDependencyCountSingleFileName));
			line++;
			// Total of analyzed files with possible dependencies
			sheet.addCell(new Number(1, line++, filesWithPossibleDependency));
			// % of analyzed files with possible dependencies
			if (possibleDependenciesMap.size() != 0) {
				sheet.addCell(new Number(1, line++, (double)filesWithPossibleDependency/(double)dependenciesMap.size(), this.floatPercentage));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Average possible dependencies per file
			if (possibleDependenciesMap.size() != 0) { 
				sheet.addCell(new Number(1, line++, (double)possibleDependencyCount/(double)dependenciesMap.size()));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Average possible dependencies per file which has possible dependencies
			if (filesWithPossibleDependency != 0) { 
				sheet.addCell(new Number(1, line++, (double)possibleDependencyCount/(double)filesWithPossibleDependency));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Max possible dependency count on a single file
			sheet.addCell(new Number(1, line, maxPossibleDependencyCountSingleFile));
			sheet.addCell(new Label(2, line, "File:"));
			sheet.addCell(new Label(3, line++, maxPossibleDependencyCountSingleFileName));
			
			// Total of functions
			line++;
			sheet.addCell(new Number(1, line++, functionCount));
			// Total of functions with directives
			sheet.addCell(new Number(1, line++, functionWithDirectiveCount));
			// % of functions with directives
			if (functionCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)functionWithDirectiveCount/(double)functionCount, this.floatPercentage));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			line++;
			// Total of functions with dependencies
			sheet.addCell(new Number(1, line++, functionWithDependencyCount));
			// % of functions with dependencies
			if (functionCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)functionWithDependencyCount/(double)functionCount, this.floatPercentage));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// % of functions with directives which has dependencies
			if (functionWithDirectiveCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)functionWithDependencyCount/(double)functionWithDirectiveCount, this.floatPercentage));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Average dependencies per function
			if (functionCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)dependencyCount/(double)functionCount));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Average dependencies per function which has dependencies
			if (functionWithDependencyCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)dependencyCount/(double)functionWithDependencyCount));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Max dependency count on a single function
			sheet.addCell(new Number(1, line, maxDependencyCountSingleFunction));
			sheet.addCell(new Label(2, line, "Function:"));
			sheet.addCell(new Label(3, line, maxDependencyCountSingleFunctionName));
			sheet.addCell(new Label(4, line, "File:"));
			sheet.addCell(new Label(5, line++, maxDependencyCountSingleFunctionFileName));
			line++;
			// Total of functions with possible dependencies
			sheet.addCell(new Number(1, line++, functionWithPossibleDependencyCount));
			// % of functions with possible dependencies
			if (functionCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)functionWithPossibleDependencyCount/(double)functionCount, this.floatPercentage));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// % of functions with directives which has posible dependencies
			if (functionWithDirectiveCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)functionWithPossibleDependencyCount/(double)functionWithDirectiveCount, this.floatPercentage));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Average possible dependencies per function
			if (functionCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)possibleDependencyCount/(double)functionCount));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Average possible dependencies per function which has possible dependencies
			if (functionWithPossibleDependencyCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)possibleDependencyCount/(double)functionWithPossibleDependencyCount));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Max possible dependency count on a single function
			sheet.addCell(new Number(1, line, maxPossibleDependencyCountSingleFunction));
			sheet.addCell(new Label(2, line, "Function:"));
			sheet.addCell(new Label(3, line, maxPossibleDependencyCountSingleFunctionName));
			sheet.addCell(new Label(4, line, "File:"));
			sheet.addCell(new Label(5, line++, maxPossibleDependencyCountSingleFunctionFileName));
			// Total functions called
			line++;
			sheet.addCell(new Number(1, line++, functionCalledCount));
			// % of functions called
			if (functionCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)functionCalledCount/(double)functionCount, this.floatPercentage));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Total function calls
			sheet.addCell(new Number(1, line++, functionCallCount));
			// Total function calls for functions with dependencies
			sheet.addCell(new Number(1, line++, functionWithDependencyCallCount));
			// Average calls per function
			if (functionCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)functionCallCount/(double)functionCount));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Average calls per function which has dependencies
			if (functionWithDependencyCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)functionWithDependencyCallCount/(double)functionWithDependencyCount));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Total of function parameters
			line++;
			sheet.addCell(new Number(1, line++, parameterCount));
			line++;
			// Total of function parameters for functions with dependencies
			sheet.addCell(new Number(1, line++, parameterOfFunctionWithDependencyCount));
			// Total of dependent function parameters
			sheet.addCell(new Number(1, line++, dependentParameterCount));
			// Average dependent parameters per function
			if (functionCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)dependentParameterCount/(double)functionCount));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Average dependent parameters per function which has dependencies
			if (functionWithDependencyCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)dependentParameterCount/(double)functionWithDependencyCount));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Average dependent parameters per function which has dependent parameters
			if (functionWithDependentParameterCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)dependentParameterCount/(double)functionWithDependentParameterCount));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			line++;
			// Total of function parameters for functions with possible dependencies
			sheet.addCell(new Number(1, line++, parameterOfFunctionWithPossibleDependencyCount));
			// Total of possible dependent function parameters
			sheet.addCell(new Number(1, line++, possibleDependentParameterCount));
			// Average possible dependent parameters per function
			if (functionCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)possibleDependentParameterCount/(double)functionCount));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Average possible dependent parameters per function which has possible dependencies
			if (functionWithPossibleDependencyCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)possibleDependentParameterCount/(double)functionWithPossibleDependencyCount));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Average possible dependent parameters per function which has possible dependent parameters
			if (functionWithPossibleDependentParameterCount != 0) { 
				sheet.addCell(new Number(1, line++, (double)possibleDependentParameterCount/(double)functionWithPossibleDependentParameterCount));
			} else {
				sheet.addCell(new Number(1, line++, 0));
			}
			// Dependencies by direction
			line++;
			sheet.addCell(new Label(1, line, "Total"));
			sheet.addCell(new Label(2, line++, "%"));
			sheet.addCell(new Number(1, line, dependencyDirectionMOCount));
			if (dependencyCount != 0) {
				sheet.addCell(new Number(2, line++, (double)dependencyDirectionMOCount/(double)dependencyCount, this.floatPercentage));
			} else {
				sheet.addCell(new Number(2, line++, 0, this.floatPercentage));
			}
			sheet.addCell(new Number(1, line, dependencyDirectionOMCount));
			if (dependencyCount != 0) {
				sheet.addCell(new Number(2, line++, (double)dependencyDirectionOMCount/(double)dependencyCount, this.floatPercentage));
			} else {
				sheet.addCell(new Number(2, line++, 0, this.floatPercentage));
			}
			sheet.addCell(new Number(1, line, dependencyDirectionOOCount));
			if (dependencyCount != 0) {
				sheet.addCell(new Number(2, line++, (double)dependencyDirectionOOCount/(double)dependencyCount, this.floatPercentage));
			} else {
				sheet.addCell(new Number(2, line++, 0, this.floatPercentage));
			}
			// Dependencies by type
			line++;
			sheet.addCell(new Label(1, line, "Total"));
			sheet.addCell(new Label(2, line++, "%"));
			sheet.addCell(new Number(1, line, dependencyTypeGlobalCount));
			if (dependencyCount != 0) {
				sheet.addCell(new Number(2, line++, (double)dependencyTypeGlobalCount/(double)dependencyCount, this.floatPercentage));
			} else {
				sheet.addCell(new Number(2, line++, 0, this.floatPercentage));
			}
			sheet.addCell(new Number(1, line, dependencyTypeFunctionCount));
			if (dependencyCount != 0) {
				sheet.addCell(new Number(2, line++, (double)dependencyTypeFunctionCount/(double)dependencyCount, this.floatPercentage));
			} else {
				sheet.addCell(new Number(2, line++, 0, this.floatPercentage));
			}
			// Dependencies by side
			line++;
			sheet.addCell(new Label(1, line, "Total"));
			sheet.addCell(new Label(2, line++, "%"));
			sheet.addCell(new Number(1, line, dependencyLSACount));
			if (dependencyCount != 0) {
				sheet.addCell(new Number(2, line++, (double)dependencyLSACount/(double)dependencyCount, this.floatPercentage));
			} else {
				sheet.addCell(new Number(2, line++, 0, this.floatPercentage));
			}
			sheet.addCell(new Number(1, line, dependencyCount - dependencyLSACount));
			if (dependencyCount != 0) {
				sheet.addCell(new Number(2, line++, (double)(dependencyCount - dependencyLSACount)/(double)dependencyCount, this.floatPercentage));
			} else {
				sheet.addCell(new Number(2, line++, 0, this.floatPercentage));
			}
			// Possible dependencies by side
			line++;
			sheet.addCell(new Label(1, line, "Total"));
			sheet.addCell(new Label(2, line++, "%"));
			sheet.addCell(new Number(1, line, possibleDependencyLSACount));
			if (possibleDependencyCount != 0) {
				sheet.addCell(new Number(2, line++, (double)possibleDependencyLSACount/(double)possibleDependencyCount, this.floatPercentage));
			} else {
				sheet.addCell(new Number(2, line++, 0, this.floatPercentage));
			}
			sheet.addCell(new Number(1, line, possibleDependencyCount - possibleDependencyLSACount));
			if (possibleDependencyCount != 0) {
				sheet.addCell(new Number(2, line++, (double)(possibleDependencyCount - possibleDependencyLSACount)/(double)possibleDependencyCount, this.floatPercentage));
			} else {
				sheet.addCell(new Number(2, line++, 0, this.floatPercentage));
			}
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*public void createSheetUniqueDependencies(WritableWorkbook workbook, Map<String, List<Dependency>> dependenciesMap) {
		WritableSheet sheet = workbook.createSheet("Unique dependencies", 4);

		// Inserting header
		String[] headers = {"FILE", "DIRECTION", "TYPE", "PRESENCE_CONDITION", "OUTER_PRESENCE_CONDITION", "INNER_PRESENCE_CONDITION", "FUNCTION", "VARIABLE"};
		for (int i = 0; i < headers.length; i++) {
			try {
				sheet.addCell(new Label(i, 0, headers[i]));
			} catch (RowsExceededException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (WriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		int i = 1;
		try {
			for (Entry<String, List<Dependency>> dependenciesList : dependenciesMap.entrySet()) {
				Set<String> uniqueDependencies = new HashSet<String>();
				for (Dependency dependency : dependenciesList.getValue()) {
					if (!uniqueDependencies.contains(dependency.getUniqueName())) {
						uniqueDependencies.add(dependency.getUniqueName());
						String[] data = (dependenciesList.getKey() + ";" + dependency).split(";");
						for (int j = 0; j < 8; j++) {
							sheet.addCell(new Label(j, i, data[j]));
						}
						i++;
					}
				}
			}
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createSheetUniqueDependenciesSummary(WritableWorkbook workbook, Map<String, List<Dependency>> dependenciesMap) {
		WritableSheet sheet = workbook.createSheet("Unique dependencies summary", 5);
	}*/
}