package global.core;

import global.core.Ast.GenerationStatus;
import global.visitor.FindFunctionDirectivesVisitor;
import global.visitor.FindFunctionParametersVisitor;
import global.visitor.FindFunctionsVisitor;
import global.visitor.FindGlobalVariableUsesVisitor;
import global.visitor.FindGlobalVariablesDeclarationsVisitor;
import global.visitor.FindVariableDeclarationVisitor;
import global.visitor.FindVariableFunctionVisitor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import tree.CompoundStatement;
import tree.FunctionDef;
import tree.Id;
import tree.Node;
import tree.Opt;
import tree.visitor.VisitorPrinter;
import tree.visitor.VisitorPrinterNames;

public class Project {
	
	private String sourcePath;
	private String stubsPath;
	private int totalFiles = 0;
	private int totalSuccessfulFiles = 0;
	private Map<String, String> skippedFiles = new HashMap<String, String>();
	private Set<String> blacklist = new HashSet<String>();
	private List<Ast> asts = new ArrayList<Ast>();
	private Map<FunctionDef, Function> functions = new HashMap<FunctionDef, Function>();
	private List<Dependency> dependencies = new ArrayList<Dependency>();
	private Map<String, Map<String, Set<String>>> maintenancePoints = new HashMap<String, Map<String, Set<String>>>();
	private Map<String, Map<String, Set<String>>> impactPoints = new HashMap<String, Map<String, Set<String>>>();
	private List<Id> globals = new ArrayList<Id>();
	
	public Project(String sourcePath, String stubsPath) {
		setSourcePath(sourcePath);
		setStubsPath(stubsPath);
	}

	public String getSourcePath() {
		return sourcePath;
	}

	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	public String getStubsPath() {
		return stubsPath;
	}

	public void setStubsPath(String stubsPath) {
		this.stubsPath = stubsPath;
	}
	
	public int getTotalFiles() {
		return totalFiles;
	}

	public void setTotalFiles(int totalFiles) {
		this.totalFiles = totalFiles;
	}

	public int getTotalSuccessfulFiles() {
		return totalSuccessfulFiles;
	}

	public void setTotalSuccessfulFiles(int totalSuccessfulFiles) {
		this.totalSuccessfulFiles = totalSuccessfulFiles;
	}
	
	public Map<String, String> getSkippedFiles() {
		return skippedFiles;
	}

	public void setSkippedFiles(Map<String, String> skippedFiles) {
		this.skippedFiles = skippedFiles;
	}

	public Set<String> getBlacklist() {
		return blacklist;
	}

	public void setBlacklist(Set<String> blacklist) {
		this.blacklist = blacklist;
	}

	public List<Ast> getAsts() {
		return asts;
	}

	public void setAsts(List<Ast> asts) {
		this.asts = asts;
	}

	public Map<FunctionDef, Function> getFunctions() {
		return functions;
	}

	public void setFunctions(Map<FunctionDef, Function> functions) {
		this.functions = functions;
	}

	public List<Dependency> getDependencies() {
		return dependencies;
	}

	public void setDependencies(List<Dependency> dependencies) {
		this.dependencies = dependencies;
	}

	public Map<String, Map<String, Set<String>>> getMaintenancePoints() {
		return maintenancePoints;
	}

	public void setMaintenancePoints(Map<String, Map<String, Set<String>>> maintenancePoints) {
		this.maintenancePoints = maintenancePoints;
	}

	public Map<String, Map<String, Set<String>>> getImpactPoints() {
		return impactPoints;
	}

	public void setImpactPoints(Map<String, Map<String, Set<String>>> impactPoints) {
		this.impactPoints = impactPoints;
	}

	public List<Id> getGlobals() {
		return globals;
	}

	public void setGlobals(List<Id> globals) {
		this.globals = globals;
	}

	public void getFile(File path) {
		if (path.isDirectory()) {
			for (File file : path.listFiles()) {
				getFile(file);
			}
		} else {
			if (!path.getName().startsWith(".")) {
				if (path.getName().endsWith(".c")) {
						//|| path.getName().endsWith(".h")) {
					System.out.println("FILE: " + path.getAbsolutePath());
					setTotalFiles(getTotalFiles() + 1);
					if (getBlacklist().contains(path.getPath())) {
						getSkippedFiles().put(path.getPath(), "File is blacklisted");
						System.out.println("File is blacklisted.");
						return;
					}
					Ast ast = new Ast(path, new File(getStubsPath()));
					GenerationStatus generationStatus;
					do {
						generationStatus = ast.generate();
					} while (generationStatus == GenerationStatus.OPTIONS_EXCEPTION);
					if (generationStatus == GenerationStatus.OK) {
						setTotalSuccessfulFiles(getTotalSuccessfulFiles() + 1);
						getAsts().add(ast);
						/*Node myAst = ast.getNode();
						myAst.accept(new VisitorPrinter(false));
						myAst.accept(new VisitorPrinterNames());*/
					} else {
						getSkippedFiles().put(path.getPath(), generationStatus.toString());
					}
				}
			}
		}
	}
	
	public void loadBlacklist() {
		File f = new File(sourcePath + "..\\blacklist.txt");
		if (f.exists()) {
			try {
				setBlacklist(new HashSet<String>(FileUtils.readLines(f)));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void analyze() {
		// Get blacklisted files (files too messy to be parsed)
		loadBlacklist();
		// Parse every .c file in the path
		getFile(new File(sourcePath));
		// Look for function definitions on every AST
		findFunctions();
		// Get global variable dependencies
		analyzeGlobalVariableDependencies();
	}
	
	private void analyzeGlobalVariableDependencies() {
		int i = 1;
		System.out.println("Looking for global variable dependencies calls on ASTs (" + getAsts().size() + ")");
		for (Ast ast : getAsts()) {
			System.out.println("AST " + i++ + "/" + getAsts().size());
			Node myAst = ast.getNode();
			int j = 1;
			
			FindGlobalVariablesDeclarationsVisitor findGlobalVariablesDeclarationsVisitor = new FindGlobalVariablesDeclarationsVisitor();
			myAst.accept(findGlobalVariablesDeclarationsVisitor);
			// Get all global variables on this AST
			List<Id> globalVariablesDeclarations = findGlobalVariablesDeclarationsVisitor.getGlobalVariablesDeclarations();
			// Keep record of all global variables
			this.globals.addAll(globalVariablesDeclarations);
			for (Id globalVariableDeclaration : globalVariablesDeclarations) {
				System.out.println("AST " + i + "/" + getAsts().size() + " - Global variable " + j++ + "/" + globalVariablesDeclarations.size());
				
				FindGlobalVariableUsesVisitor findGlobalVariableUsesVisitor = new FindGlobalVariableUsesVisitor(globalVariableDeclaration);
				myAst.accept(findGlobalVariableUsesVisitor);
				// Get all uses for this global variable
				List<Id> globalVariableUses = findGlobalVariableUsesVisitor.getGlobalVariableUses();
				
				for (Id globalVariableUse : globalVariableUses) {
								
					FindVariableFunctionVisitor findVariableFunctionVisitor = new FindVariableFunctionVisitor();
					globalVariableUse.accept(findVariableFunctionVisitor);
					
					FunctionDef functionDef = findVariableFunctionVisitor.getFunctionDef();
					// If global variable use is outside a function, skip it
					if (functionDef == null) {
						continue;
					}
					
					Function function = getFunctions().get(functionDef);
					// Get the scope of the expression containing the global variable
					List<CompoundStatement> scope = findVariableFunctionVisitor.getScope();
					// Looks for a homonymous variable declared within the scope
					FindVariableDeclarationVisitor findVariableDeclarationVisitor = new FindVariableDeclarationVisitor(globalVariableUse, scope, true);
					functionDef.accept(findVariableDeclarationVisitor);
					// No homonymous variable found
					if (!(findVariableDeclarationVisitor.isFound())) {
						// Keep record of global variables accesses
						function.getGlobalReferences().add(globalVariableUse);
						// Non equivalent presence condition
						if ((!(globalVariableDeclaration.getPresenceCondition().equivalentTo(globalVariableUse.getPresenceCondition()))) &&
							// Combined presence conditions are not a contradiction
							(!(globalVariableDeclaration.getPresenceCondition().and(globalVariableUse.getPresenceCondition()).isContradiction()))) {
							Dependency dependency = new GlobalVariableDependency(function, globalVariableDeclaration.getPresenceCondition().and(globalVariableUse.getPresenceCondition()), globalVariableUse.getPresenceCondition(), globalVariableDeclaration.getPresenceCondition(), globalVariableUse, globalVariableDeclaration);
							// Discard any dependency on stubs file
							if ((dependency.getInnerFile().equals(this.stubsPath)) || (dependency.getOuterFile().equals(this.stubsPath))) {
								continue;
							}
							getDependencies().add(dependency);
							function.getDependencies().add(dependency);
						}
						//functionMetricsMap.get(dependency.getFunction().getName()).getDependencies().add(dependency);
					} else {
						// A local variable has the same name as the parameter
						// Non equivalent presence condition
						if ((!(findVariableDeclarationVisitor.getPresenceCondition().equivalentTo(globalVariableUse.getPresenceCondition()))) &&
							// Combined presence conditions are not a contradiction
							(!(globalVariableDeclaration.getPresenceCondition().and(globalVariableUse.getPresenceCondition()).and(findVariableDeclarationVisitor.getPresenceCondition())).isContradiction())) {
							// Keep record of global variables accesses
							function.getGlobalReferences().add(globalVariableUse);
							// Presence condition now depends on the non-declaration of the local variable 
							Dependency dependency = new GlobalVariableDependency(function, globalVariableDeclaration.getPresenceCondition().and(globalVariableUse.getPresenceCondition()).and(findVariableDeclarationVisitor.getPresenceCondition()), globalVariableUse.getPresenceCondition(), globalVariableDeclaration.getPresenceCondition(), globalVariableUse, globalVariableDeclaration);
							// Discard any dependency on stubs file
							if ((dependency.getInnerFile().equals(this.stubsPath)) || (dependency.getOuterFile().equals(this.stubsPath))) {
								continue;
							}
							getDependencies().add(dependency);
							function.getDependencies().add(dependency);
							//functionMetricsMap.get(dependency.getFunction().getName()).getDependencies().add(dependency);
						}
					}
				}
			}
		}
	}

	public void findFunctions() {
		int i = 1;
		System.out.println("Looking for function definitions on ASTs (" + getAsts().size() + ")");
		for (Ast ast : getAsts()) {
			System.out.println("AST " + i++ + "/" + getAsts().size());
			Node myAst = ast.getNode();
			FindFunctionsVisitor findFunctionsVisitor = new FindFunctionsVisitor();
			myAst.accept(findFunctionsVisitor);
			// Get all function definitions on this AST
			List<FunctionDef> functionDefs = findFunctionsVisitor.getFunctions();
			System.out.println(functionDefs.size() + " function definitions found");
			// Create a function object for each function definition
			for (FunctionDef functionDef : functionDefs) {
				FindFunctionDirectivesVisitor findFunctionDirectivesVisitor = new FindFunctionDirectivesVisitor(functionDef);
				functionDef.accept(findFunctionDirectivesVisitor);
				// Get function directives
				Set<Opt> functionDirectives = findFunctionDirectivesVisitor.getFunctionDirectives();
				// Get function inner directives (ifdefs inside function)
				Set<Opt> functionOpts = findFunctionDirectivesVisitor.getFunctionOpts();
				FindFunctionParametersVisitor findFunctionParametersVisitor = new FindFunctionParametersVisitor();
				functionDef.accept(findFunctionParametersVisitor);
				// Get function parameters
				List<Id> functionParameters = findFunctionParametersVisitor.getFunctionParameters();
				Function function = new Function(functionDef, functionParameters);
				function.setDirectives(functionDirectives);
				function.setOpts(functionOpts);
				getFunctions().put(functionDef, function);
			}
		}
	}

	public void generateMetrics() {
		// Get every call to a function with dependency
		measureMaintenancePoints();
		// Get every reference to a variable which causes dependency inside a function
		measureImpactPoints();
	}	
	
	private void measureImpactPoints() {
		// Acrescentar depend�ncias tanto nas fun��es caller como callee
		for (Dependency dependency : getDependencies()) {
			Function function = dependency.getCallee();
			String filename = function.getFunctionDef().getPositionFrom().getFile();
			// Reference of the variable in a dependency
			String reference = dependency.getInnerFile() + ":" + dependency.getInnerPosition();
			// A impact point contains the filename, the function and the references for variables in a dependency
			if (getImpactPoints().containsKey(filename)) {
				if (getImpactPoints().get(filename).containsKey(function.getName())) {
					getImpactPoints().get(filename).get(function.getName()).add(reference);
				} else {
					Set<String> references = new HashSet<String>();
					references.add(reference);
					getImpactPoints().get(filename).put(function.getName(), references);
				}
			} else {
				Map<String, Set<String>> functionReferences = new HashMap<String, Set<String>>();
				Set<String> references = new HashSet<String>();
				references.add(reference);
				functionReferences.put(function.getName(), references);
				getImpactPoints().put(filename, functionReferences);
			}
		}
	}

	private void measureMaintenancePoints() {
		for (Dependency dependency : getDependencies()) {
			if (!(dependency instanceof FunctionCallDependency)) {
				continue;
			}
			Function function = ((FunctionCallDependency) dependency).getCaller();
			String filename = function.getFunctionDef().getPositionFrom().getFile();
			// Entry point a function call dependency
			String entryPoint = dependency.getOuterFile() + ":" + dependency.getOuterPosition();
			// A maintenance point contains the filename, the function and its entry points
			if (getMaintenancePoints().containsKey(filename)) {
				if (getMaintenancePoints().get(filename).containsKey(function.getName())) {
					getMaintenancePoints().get(filename).get(function.getName()).add(entryPoint);
				} else {
					Set<String> entryPoints = new HashSet<String>();
					entryPoints.add(entryPoint);
					getMaintenancePoints().get(filename).put(function.getName(), entryPoints);
				}
			} else {
				Map<String, Set<String>> functionEntryPoints = new HashMap<String, Set<String>>();
				Set<String> entryPoints = new HashSet<String>();
				entryPoints.add(entryPoint);
				functionEntryPoints.put(function.getName(), entryPoints);
				getMaintenancePoints().put(filename, functionEntryPoints);
			}
			
		}
	}
}
