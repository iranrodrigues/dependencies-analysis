package global.core;

import tree.FunctionCall;
import tree.Id;
import tree.Node;
import de.fosd.typechef.featureexpr.FeatureExpr;

public class FunctionCallDependency extends Dependency {
	private FunctionCall functionCall;
	private Function caller;
	private int parameterOrder;
	private Node parameter;

	public FunctionCallDependency(Function function,
			FeatureExpr presenceCondition, FeatureExpr innerPresenceCondition,
			FeatureExpr outerPresenceCondition, Id variable, FunctionCall functionCall) {
		super(function, presenceCondition, innerPresenceCondition,
				outerPresenceCondition, variable);
		this.functionCall = functionCall;
	}

	public FunctionCall getFunctionCall() {
		return functionCall;
	}
	
	public Function getCaller() {
		return caller;
	}
	
	public void setCaller(Function caller) {
		this.caller = caller;
	}
	
	public int getParameterOrder() {
		return parameterOrder;
	}

	public void setParameterOrder(int parameterOrder) {
		this.parameterOrder = parameterOrder;
	}
	
	public Node getParameter() {
		return parameter;
	}

	public void setParameter(Node parameter) {
		this.parameter = parameter;
	}

	public String getOuterFile() {
		return this.functionCall.getPositionFrom().getFile().substring(5);
	}
	
	public String getOuterPosition() {
		return this.functionCall.getPositionFrom().getLine() + ":" + this.functionCall.getPositionFrom().getColumn();
	}
	
	public String toString() {
		return
			getDirection() + ";" +
			getClass().getSimpleName() + ";" +
			getPresenceCondition().toString() + ";" +
			getOuterPresenceCondition().toString() + ";" +
			getInnerPresenceCondition().toString() + ";" +
			getCallee().getName() + ";" +
			getVariable().getName() + ";" +
			isLeftSideAssignment() + ";" +
			getOuterFile() + ";" +
			getOuterPosition() + ";" +
			getInnerFile() + ";" +
			getInnerPosition() + ";" +
			getParameterOrder() + ";" +
			getParameter();
	}
	
}
