package global.core;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tree.AtomicNamedDeclarator;
import tree.FunctionCall;
import tree.FunctionDef;
import tree.Id;
import tree.Opt;

public class Function {
	private FunctionDef functionDef;
	private String name;
	private List<Id> parameters = new ArrayList<Id>();
	private List<FunctionCall> functionCalls = new ArrayList<FunctionCall>();
	private Set<Opt> directives = new HashSet<Opt>();
	private List<Dependency> dependencies = new ArrayList<Dependency>();
	private List<Id> globalReferences = new ArrayList<Id>();
	private Set<Opt> opts = new HashSet<Opt>();
	
	public Function(FunctionDef functionDef, List<Id> parameters) {
		this.functionDef = functionDef;
		this.parameters = parameters;
		
		for (int i = 0; i < functionDef.getChildren().size(); i++) {
			if (functionDef.getChildren().get(i) instanceof AtomicNamedDeclarator) {
				for (int j = 0; j < functionDef.getChildren().get(i).getChildren().size(); j++) {
					if (functionDef.getChildren().get(i).getChildren().get(j) instanceof Id) {
						this.name = ((Id) functionDef.getChildren().get(i).getChildren().get(j)).getName();
					}
				}
			}
		}
	}

	public FunctionDef getFunctionDef() {
		return functionDef;
	}

	public void setFunctionDef(FunctionDef functionDef) {
		this.functionDef = functionDef;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Id> getParameters() {
		return parameters;
	}

	public void setParameters(List<Id> parameters) {
		this.parameters = parameters;
	}

	public List<FunctionCall> getFunctionCalls() {
		return functionCalls;
	}

	public void setFunctionCalls(List<FunctionCall> functionCalls) {
		this.functionCalls = functionCalls;
	}

	public Set<Opt> getDirectives() {
		return directives;
	}

	public void setDirectives(Set<Opt> directives) {
		this.directives = directives;
	}

	public List<Dependency> getDependencies() {
		return dependencies;
	}

	public void setDependencies(List<Dependency> dependencies) {
		this.dependencies = dependencies;
	}
	
	public List<Id> getGlobalReferences() {
		return globalReferences;
	}

	public void setGlobalReferences(List<Id> globalReferences) {
		this.globalReferences = globalReferences;
	}
	
	public Set<Opt> getOpts() {
		return opts;
	}

	public void setOpts(Set<Opt> opts) {
		this.opts = opts;
	}
}
