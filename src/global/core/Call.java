package global.core;

import java.util.List;

import tree.Node;

public class Call {
	
	private tree.FunctionCall functionCall;

	public tree.FunctionCall getFunctionCall() {
		return functionCall;
	}

	public void setFunctionCall(tree.FunctionCall functionCall) {
		this.functionCall = functionCall;
	}
	
	public Call(tree.FunctionCall functionCall) {
		this.functionCall = functionCall;
	}

	public Node getParameter(int parameterOrder) {
		List<Node> parameters = getFunctionCall().getChildren().get(0).getChildren();
		int i = 0;
		for (Node parameter : parameters) {
			if (i == parameterOrder) {
				return parameter;
			}
			i++;
		}
		return null;
	}

}
