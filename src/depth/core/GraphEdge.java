package depth.core;

public class GraphEdge {
	
	private GraphNode callee;
	private GraphNode caller;
	
	public GraphNode getCallee() {
		return callee;
	}

	public void setCallee(GraphNode callee) {
		this.callee = callee;
	}

	public GraphNode getCaller() {
		return caller;
	}

	public void setCaller(GraphNode caller) {
		this.caller = caller;
	}

	public GraphEdge(GraphNode callee, GraphNode caller) {
		this.callee = callee;
		this.caller = caller;
	}

}
