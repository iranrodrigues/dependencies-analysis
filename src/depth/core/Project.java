package depth.core;

import interprocedural.visitor.FindFunctionParametersVisitor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import tree.CompoundStatement;
import tree.ExprList;
import tree.FunctionCall;
import tree.FunctionDef;
import tree.Id;
import tree.Node;
import tree.Opt;
import de.fosd.typechef.featureexpr.FeatureExpr;
import depth.core.Ast.GenerationStatus;
import depth.visitor.FindFunctionCallArgumentsVisitor;
import depth.visitor.FindFunctionCallsVisitor;
import depth.visitor.FindFunctionDefVisitor;
import depth.visitor.FindFunctionDirectivesVisitor;
import depth.visitor.FindFunctionParameterUsesVisitor;
import depth.visitor.FindFunctionsVisitor;
import depth.visitor.FindVariableDeclarationVisitor;
import depth.visitor.FindVariablesInFunctionCallArgumentsVisitor;

public class Project {
	
	private String sourcePath;
	private String stubsPath;
	private int totalFiles = 0;
	private int totalSuccessfulFiles = 0;
	private Map<String, String> skippedFiles = new HashMap<String, String>();
	private Set<String> blacklist = new HashSet<String>();
	private List<Ast> asts = new ArrayList<Ast>();
	private Map<FunctionDef, Function> functions = new HashMap<FunctionDef, Function>();
	private List<Dependency> dependencies = new ArrayList<Dependency>();
	private Map<String, Map<String, Set<String>>> maintenancePoints = new HashMap<String, Map<String, Set<String>>>();
	private Map<String, Map<String, Set<String>>> impactPoints = new HashMap<String, Map<String, Set<String>>>();
	private Set<Map<Function, Map<Function, Integer>>> dependencyDepths = new HashSet<Map<Function, Map<Function, Integer>>>();
	private List<Chain> chainedFunctions = new ArrayList<Chain>();
	private Map<String, List<FunctionCall>> functionCalls = new HashMap<String, List<FunctionCall>>();
	private List<List<String>> depthsStack = new ArrayList<List<String>>();
	private Map<Dependency, List<GraphNode>> maximumDepthNodes = new HashMap<Dependency, List<GraphNode>>();
	private Map<Dependency, List<List<GraphNode>>> dependencyPaths = new HashMap<Dependency, List<List<GraphNode>>>();
	
	private Map<GraphNode, Set<GraphNode>> graphEdges = new HashMap<GraphNode, Set<GraphNode>>();
	private Set<GraphNode> visitedGraphNodes = new HashSet<GraphNode>();
	private BufferedWriter bw;
	
	public Project(String sourcePath, String stubsPath) {
		setSourcePath(sourcePath);
		setStubsPath(stubsPath);
	}

	public String getSourcePath() {
		return sourcePath;
	}

	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	public String getStubsPath() {
		return stubsPath;
	}

	public void setStubsPath(String stubsPath) {
		this.stubsPath = stubsPath;
	}
	
	public int getTotalFiles() {
		return totalFiles;
	}

	public void setTotalFiles(int totalFiles) {
		this.totalFiles = totalFiles;
	}

	public int getTotalSuccessfulFiles() {
		return totalSuccessfulFiles;
	}

	public void setTotalSuccessfulFiles(int totalSuccessfulFiles) {
		this.totalSuccessfulFiles = totalSuccessfulFiles;
	}
	
	public Map<String, String> getSkippedFiles() {
		return skippedFiles;
	}

	public void setSkippedFiles(Map<String, String> skippedFiles) {
		this.skippedFiles = skippedFiles;
	}

	public Set<String> getBlacklist() {
		return blacklist;
	}

	public void setBlacklist(Set<String> blacklist) {
		this.blacklist = blacklist;
	}

	public List<Ast> getAsts() {
		return asts;
	}

	public void setAsts(List<Ast> asts) {
		this.asts = asts;
	}

	public Map<FunctionDef, Function> getFunctions() {
		return functions;
	}

	public void setFunctions(Map<FunctionDef, Function> functions) {
		this.functions = functions;
	}

	public List<Dependency> getDependencies() {
		return dependencies;
	}

	public void setDependencies(List<Dependency> dependencies) {
		this.dependencies = dependencies;
	}

	public Map<String, Map<String, Set<String>>> getMaintenancePoints() {
		return maintenancePoints;
	}

	public void setMaintenancePoints(Map<String, Map<String, Set<String>>> maintenancePoints) {
		this.maintenancePoints = maintenancePoints;
	}

	public Map<String, Map<String, Set<String>>> getImpactPoints() {
		return impactPoints;
	}

	public void setImpactPoints(Map<String, Map<String, Set<String>>> impactPoints) {
		this.impactPoints = impactPoints;
	}

	public Set<Map<Function, Map<Function, Integer>>> getDependencyDepths() {
		return dependencyDepths;
	}

	public void setDependencyDepths(
			Set<Map<Function, Map<Function, Integer>>> dependencyDepths) {
		this.dependencyDepths = dependencyDepths;
	}

	public List<Chain> getChainedFunctions() {
		return chainedFunctions;
	}

	public void setChainedFunctions(List<Chain> chainedFunctions) {
		this.chainedFunctions = chainedFunctions;
	}
	
	public Map<Dependency, List<GraphNode>> getMaximumDepthNodes() {
		return maximumDepthNodes;
	}
	
	public Map<Dependency, List<List<GraphNode>>> getDependencyPaths() {
		return dependencyPaths;
	}

	public void getFile(File path) {
		if (path.isDirectory()) {
			for (File file : path.listFiles()) {
				getFile(file);
			}
		} else {
			if (!path.getName().startsWith(".")) {
				if (path.getName().endsWith(".c")) {
						//|| path.getName().endsWith(".h")) {
					System.out.println("FILE: " + path.getAbsolutePath());
					setTotalFiles(getTotalFiles() + 1);
					if (getBlacklist().contains(path.getPath())) {
						getSkippedFiles().put(path.getPath(), "File is blacklisted");
						System.out.println("File is blacklisted.");
						return;
					}
					Ast ast = new Ast(path, new File(getStubsPath()));
					GenerationStatus generationStatus;
					do {
						generationStatus = ast.generate();
					} while (generationStatus == GenerationStatus.OPTIONS_EXCEPTION);
					if (generationStatus == GenerationStatus.OK) {
						setTotalSuccessfulFiles(getTotalSuccessfulFiles() + 1);
						getAsts().add(ast);
						//Node myAst = ast.getNode();
						/*FindVariableDefinitionVisitor findVariableDefinitionVisitor = new FindVariableDefinitionVisitor();
						myAst.accept(findVariableDefinitionVisitor);
						for (Id id : findVariableDefinitionVisitor.getLocalVariables()) {
							System.out.println("Variable definition: " + id.getName() + " at " + id.getPositionFrom().getLine() + ":" + id.getPositionFrom().getColumn());
						}*/
						//myAst.accept(new VisitorPrinter(false));
						//myAst.accept(new VisitorPrinterNames());
						//System.exit(0);
					} else {
						getSkippedFiles().put(path.getPath(), generationStatus.toString());
					}
				}
			}
		}
	}
	
	public void loadBlacklist() {
		File f = new File(sourcePath + "..\\blacklist.txt");
		if (f.exists()) {
			try {
				setBlacklist(new HashSet<String>(FileUtils.readLines(f)));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void analyze() {
		// Get blacklisted files (files too messy to be parsed)
		loadBlacklist();
		// Parse every .c file in the path
		getFile(new File(sourcePath));
		// Look for function definitions on every AST
		findFunctions();
		// Look for function calls for every function definition on every AST
		findFunctionCalls();
		// Get global variable dependencies
		//analyzeGlobalVariableDependencies();
		// Look for function call dependencies in every function
		analyzeFunctionCallDependencies();
		createDAG();
	}
	
	public Node findParent(Node node, String className) {
		Node parent = node.getParent();
		while (!(parent.getClass().getCanonicalName().equals(className))) {
			parent = parent.getParent();
			if (parent == null) {
				break;
			}
		}
		return parent;
	}
	
	public int getParameterPosition(FunctionDef functionDef, Id parameter) {
		if (functionDef == null) {
			return -1;
		}
		String parameterName = parameter.getName();
		depth.visitor.FindFunctionParametersVisitor findFunctionParametersVisitor = new depth.visitor.FindFunctionParametersVisitor();
		functionDef.accept(findFunctionParametersVisitor);
		List<Id> parameters = findFunctionParametersVisitor.getFunctionParameters();
		int i = -1;
		for (Id param : parameters) {
			i++;
			if (param.getName().equals(parameterName)) {
				return i;
			}
		}
		return -1;
	}
	
	private void createDAG() {
		Set<GraphEdgeHelper> graphEdgeHelpers = new HashSet<GraphEdgeHelper>();
		//Set<GraphEdge> graphEdges = new HashSet<GraphEdge>();
		Set<GraphNode> graphNodes = new HashSet<GraphNode>();
		List<FunctionCall> functionCalls = new ArrayList<FunctionCall>();
		int i = 0;
		
		for (Ast ast : getAsts()) {
			System.out.println("AST " + ++i + "/" + getAsts().size());
			Node myAst = ast.getNode();
			
			FindFunctionCallsVisitor findFunctionCallsVisitor = new FindFunctionCallsVisitor();
			myAst.accept(findFunctionCallsVisitor);
			// Get all function calls from all ASTs
			functionCalls.addAll(findFunctionCallsVisitor.getFunctionCalls());
		}
		
		for (FunctionCall functionCall : functionCalls) {
			ExprList exprList = null;
			// Get function call argument list
			for (int j = 0; j < functionCall.getChildren().size(); j++) {
				if (functionCall.getChildren().get(j) instanceof ExprList) {
					exprList = (ExprList) functionCall.getChildren().get(j);
				}
			}
			if (exprList == null) {
				continue;
			}
			// Get the functionDef outside the function call
			FunctionDef functionDef = (FunctionDef) findParent(functionCall, "tree.FunctionDef");
			if (!(functions.containsKey(functionDef))) {
				continue;
			}
			// For each argument in the function call...
			for (int j = 0; j < exprList.getChildren().size(); j++) {
				Node exprListChild = exprList.getChildren().get(j);
				FindVariablesInFunctionCallArgumentsVisitor findVariablesInFunctionCallArgumentsVisitor = new FindVariablesInFunctionCallArgumentsVisitor();
				exprListChild.accept(findVariablesInFunctionCallArgumentsVisitor);
				// Get the variables used as arguments in the function call
				List<Id> variables = findVariablesInFunctionCallArgumentsVisitor.getFunctionCallVariables();
				
				for (Id variable : variables) {
					int parameterPosition = getParameterPosition(functionDef, variable);
					// If the variable is found on the function parameter list
					//if (parameterPosition != -1) {
						// Create a new graph node
						//System.out.println("Variable " + variable.getName() + " found at position " + j + " of call " + ((Id) functionCall.getParent().getChildren().get(0)).getName());
						GraphNode graphNode1 = new GraphNode(functionCall, j, variable.getPresenceCondition());
						GraphNode graphNode2 = new GraphNode(functions.get(functionDef).getName(), parameterPosition, functionDef.getPresenceCondition(), functionDef);
						graphNodes.add(graphNode1);
						try {
						graphNodes.add(graphNode2);
						} catch (Exception e) {
							System.out.println(graphNode2);
						}
						if (!(graphEdges.containsKey(graphNode1))) {
							graphEdges.put(graphNode1, new HashSet<GraphNode>());
						}
						graphEdges.get(graphNode1).add(graphNode2);
						//System.out.println(graphNode1 + " is called by " + graphEdges.get(graphNode1));
						//GraphEdgeHelper graphEdgeHelper = new GraphEdgeHelper(graphNode, parameterPosition, functionDef);
						//graphEdgeHelpers.add(graphEdgeHelper);
						
					/*} else {
						// Experimental
						// Create a new graph node
						GraphNode graphNode = new GraphNode(functionCall, j, variable.getPresenceCondition());
						graphNodes.add(graphNode);
						 
						GraphEdgeHelper graphEdgeHelper = new GraphEdgeHelper(graphNode, parameterPosition, functionDef);
						graphEdgeHelpers.add(graphEdgeHelper);
					}*/
				}
				/*if (variables.size() == 0) {
					// Experimental
					// Create a new graph node
					GraphNode graphNode = new GraphNode(functionCall, j, exprListChild.getPresenceCondition());
					graphNodes.add(graphNode);
					 
					GraphEdgeHelper graphEdgeHelper = new GraphEdgeHelper(graphNode, -1, functionDef);
					graphEdgeHelpers.add(graphEdgeHelper);
				}*/
			}			
		}
		
		/*for (GraphEdgeHelper graphEdgeHelper: graphEdgeHelpers) {
			/*if (!(functions.containsKey(graphEdgeHelper.getFunctionDef()))) {
				continue;
			}
			if (functions.get(graphEdgeHelper.getFunctionDef()) == null) {
				continue;
			}*/
			//if (functions.get(graphEdgeHelper.getFunctionDef()).getName() == null) {
				//continue;
			//}
			// Experimental
			/*if (graphEdgeHelper.getParameterPosition() == -1) {
				if (!(graphEdges.containsKey(graphEdgeHelper.getGraphNode()))) {
					graphEdges.put(graphEdgeHelper.getGraphNode(), new HashSet<GraphNode>());
				}
				GraphNode graphNode = new GraphNode(functions.get(graphEdgeHelper.getFunctionDef()).getName(), -1, graphEdgeHelper.getFunctionDef().getPresenceCondition(), graphEdgeHelper.getFunctionDef());
				graphEdges.get(graphEdgeHelper.getGraphNode()).add(graphNode);
				System.out.println("HACK: " + graphEdgeHelper.getGraphNode().getFunctionName() + "(" + graphEdgeHelper.getParameterPosition() + ") is called by " + graphEdges.get(graphEdgeHelper.getGraphNode()));
			} else*/ /*{
				for (GraphNode graphNode : graphNodes) {
					//System.out.println("Edge: " + functions.get(graphEdgeHelper.getFunctionDef()).getName() + " Node: " + graphNode.getFunctionName());
					
					if (functions.get(graphEdgeHelper.getFunctionDef()).getName().equals(graphNode.getFunctionName()) &&
							((graphEdgeHelper.getParameterPosition() == -1)
									|| (graphEdgeHelper.getParameterPosition() == graphNode.getParameterPosition()))) {
						//GraphEdge graphEdge = new GraphEdge(graphEdgeHelper.getGraphNode(), graphNode);
						if (!(graphEdges.containsKey(graphEdgeHelper.getGraphNode()))) {
							graphEdges.put(graphEdgeHelper.getGraphNode(), new HashSet<GraphNode>());
						}
						graphEdges.get(graphEdgeHelper.getGraphNode()).add(graphNode);
						System.out.println(graphEdgeHelper.getGraphNode().getFunctionName() + "(" + graphEdgeHelper.getGraphNode().getParameterPosition() + ") is called by " + graphEdges.get(graphEdgeHelper.getGraphNode()));
						//System.out.println(functions.get(graphEdgeHelper.getFunctionDef()).getName() + " " + graphEdgeHelper.getParameterPosition() + " sort of match " + graphNode.getFunctionName() + " " + graphNode.getParameterPosition());
					} /*else {
						System.out.println(functions.get(graphEdgeHelper.getFunctionDef()).getName() + " " + graphEdgeHelper.getParameterPosition() + " do not match " + graphNode.getFunctionName() + " " + graphNode.getParameterPosition());
						
					}*/
					
			/*	}
			}	
		}*/
		
	}
	
	
	public void initializeFile() {
		File file = new File("C:\\Users\\Iran\\Google Drive\\mestrado\\projeto\\interprocedural\\spreadsheets\\depth\\lua.csv");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		FileWriter fw = null;
		try {
			fw = new FileWriter(file.getAbsoluteFile());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		bw = new BufferedWriter(fw);
		
		// Inserting header
		String headers = "DEPTH";
		try {
			bw.write(headers);
			bw.newLine();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		/*List<List<GraphNode>> paths = new ArrayList<List<GraphNode>>();
		FunctionCallDependency dependency = null;
		int depth;
		for (Entry<Dependency, List<List<GraphNode>>> dependencyPaths : getProject().getDependencyPaths().entrySet()) {
			dependency = (FunctionCallDependency) dependencyPaths.getKey();
			paths = dependencyPaths.getValue();
			for (List<GraphNode> maxPath : paths) {
				depth = maxPath.size() - 1;
				try {
					bw.write(String.valueOf(depth));
					bw.newLine();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		try {
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	
	public void modifiedDfs(Dependency dependency, GraphNode graphNode, FeatureExpr combinedPC, List<GraphNode> nodeList) {
		//System.out.println("Visiting " + graphNode);
		//System.out.println("Current depth: " + nodeList.size());
		//System.out.println(" > " + nodeList);
		//visitedGraphNodes.add(graphNode);
		/*if ((dependencyPaths.containsKey(dependency)) && (dependencyPaths.get(dependency).size() > 150000)) {
			return;
		}*/
		if (!(graphEdges.containsKey(graphNode))) {
			return;
		}
		//System.out.println("Neighbors: " + graphEdges.get(graphNode));
		//List<GraphNode> newList = new ArrayList<GraphNode>();
		//System.out.println("Neighbors: " + graphEdges.get(graphNode).size());
		List<GraphNode> newList = new ArrayList<GraphNode>();
		for (GraphNode neighbor : graphEdges.get(graphNode)) {
			if (nodeList.contains(neighbor)) {
				continue;
			}
			if (neighbor.equals(graphNode)) {
				continue;
			}
			if (combinedPC.and(neighbor.getPresenceCondition()).isContradiction()) {
				continue;
			}
			//System.out.println(graphNode + " neighbor: " + neighbor);
			//newList = new ArrayList<GraphNode>(nodeList);
			newList.clear();
			newList.addAll(nodeList);
			newList.add(neighbor);
			//System.out.println(" > " + newList);
			//System.out.println("dfs until now: " + newList);
			//System.out.println("Possible neighbors: " + graphEdges.get(graphNode));
			if (!(dependencyPaths.containsKey(dependency))) {
				dependencyPaths.put(dependency, new ArrayList<List<GraphNode>>());
			}
			dependencyPaths.get(dependency).add(newList);
			/*try {
				bw.write(String.valueOf(newList.size()));
				bw.newLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			//System.out.println(dependencyPaths.get(dependency).size());
			//System.out.println("Depth: " + newList.size());
			modifiedDfs(dependency, neighbor, combinedPC.and(neighbor.getPresenceCondition()), newList);
		}
	}
	
	public void dfs(Dependency dependency, GraphNode graphNode, FeatureExpr combinedPC, List<GraphNode> nodeList) {
		//System.out.println("Visiting " + graphNode);
		//System.out.println("Current depth: " + nodeList.size());
		//System.out.println(" > " + nodeList);
		visitedGraphNodes.add(graphNode);
		if (!(graphEdges.containsKey(graphNode))) {
			return;
		}
		//System.out.println("Neighbors: " + graphEdges.get(graphNode));
		List<GraphNode> newList = new ArrayList<GraphNode>();
		for (GraphNode neighbor : graphEdges.get(graphNode)) {
			if (!(visitedGraphNodes.contains(neighbor))) {
				if (combinedPC.and(neighbor.getPresenceCondition()).isContradiction()) {
					continue;
				}
				//System.out.println(graphNode + " neighbor: " + neighbor);
				newList.clear();
				newList.addAll(nodeList);
				newList.add(neighbor);
				//System.out.println(" > " + newList);
				//System.out.println("dfs until now: " + newList);
				//System.out.println("Possible neighbors: " + graphEdges.get(graphNode));
				if (!(dependencyPaths.containsKey(dependency))) {
					dependencyPaths.put(dependency, new ArrayList<List<GraphNode>>());
				}
				dependencyPaths.get(dependency).add(newList);
				dfs(dependency, neighbor, combinedPC.and(neighbor.getPresenceCondition()), newList);
				
			}
		}
	}

	private void analyzeFunctionCallDependencies() {
		int i = 1;
		System.out.println("Looking for function call dependencies in functions (" + getFunctions().size() + ")");
		for (Entry<FunctionDef, Function> functionMap : getFunctions().entrySet()) {
			Function function = functionMap.getValue();
			//System.out.println("Function " + i++ + "/" + getFunctions().size() + " (" + function.getName() + ")");
			// For each parameter of this function...
			int parameterOrder = 0;
			for (Id parameter : function.getParameters()) {
				FindFunctionParameterUsesVisitor findFunctionParameterUsesVisitor = new FindFunctionParameterUsesVisitor(parameter);
				function.getFunctionDef().accept(findFunctionParameterUsesVisitor);
				// Get all uses of this parameter inside function
				List<Id> functionParameterUses = findFunctionParameterUsesVisitor.getFunctionParameterUses();
				for (Id functionParameterUse : functionParameterUses) {
					// Get the function scope
					List<CompoundStatement> scope = new ArrayList<CompoundStatement>();
					for (int k = 0; k < function.getFunctionDef().getChildren().size(); k++) {
						if (function.getFunctionDef().getChildren().get(k) instanceof CompoundStatement) {
							scope.add((CompoundStatement) function.getFunctionDef().getChildren().get(k));
						}
					}
					FindVariableDeclarationVisitor findVariableDeclarationVisitor = new FindVariableDeclarationVisitor(functionParameterUse, scope, false);
					// Search for homonymous variables
					function.getFunctionDef().accept(findVariableDeclarationVisitor);
					if (!(findVariableDeclarationVisitor.isFound())) {
						// No homonymous variable found
						// For each call, check if there is a dependency
						for (FunctionCall functionCall : function.getFunctionCalls()) {
							// Find the argument
							
							// Non equivalent presence condition
							if ((!(functionCall.getPresenceCondition().equivalentTo(functionParameterUse.getPresenceCondition())) &&
								// Presence condition in function call *and* parameter use cannot be a contradiction
								(!(functionCall.getPresenceCondition().and(functionParameterUse.getPresenceCondition()).isContradiction())))) {
								Dependency dependency = new FunctionCallDependency(function, functionCall.getPresenceCondition().and(functionParameterUse.getPresenceCondition()), functionParameterUse.getPresenceCondition(), functionCall.getPresenceCondition(), functionParameterUse, functionCall);
								((FunctionCallDependency) dependency).setParameterOrder(parameterOrder);
								Call call = new Call(((FunctionCallDependency) dependency).getFunctionCall());
								if (call.getParameter(parameterOrder) == null) {
									continue;
								}
								((FunctionCallDependency) dependency).setParameter(call.getParameter(parameterOrder));
								// Discard any dependency on stubs file
								if ((dependency.getInnerFile().equals(getStubsPath())) || (dependency.getOuterFile().equals(getStubsPath()))) {
									continue;
								}
								FindFunctionDefVisitor findFunctionDefVisitor = new FindFunctionDefVisitor();
								functionCall.accept(findFunctionDefVisitor);
								Function caller = getFunctions().get(findFunctionDefVisitor.getFunctionDef());
								((FunctionCallDependency) dependency).setCaller(caller);
								function.getDependencies().add(dependency);
								getDependencies().add(dependency);
							}
						}
					} else {
						// A local variable has the same name as the parameter
						// For each call, check if there is a dependency
						for (FunctionCall functionCall : function.getFunctionCalls()) {
							// Non equivalent presence condition
							if ((!(functionCall.getPresenceCondition().equivalentTo(functionParameterUse.getPresenceCondition())) &&
								// Presence condition in function call *and* parameter use *and (not)* homonymous variable declaration cannot be a contradiction
								(!(functionCall.getPresenceCondition().and(functionParameterUse.getPresenceCondition()).and(findVariableDeclarationVisitor.getPresenceCondition()).isContradiction())))) {
									Dependency dependency = new FunctionCallDependency(function, functionCall.getPresenceCondition().and(functionParameterUse.getPresenceCondition()).and(findVariableDeclarationVisitor.getPresenceCondition()), functionParameterUse.getPresenceCondition(), functionCall.getPresenceCondition(), functionParameterUse, functionCall);
									((FunctionCallDependency) dependency).setParameterOrder(parameterOrder);
									Call call = new Call(((FunctionCallDependency) dependency).getFunctionCall());
									((FunctionCallDependency) dependency).setParameter(call.getParameter(parameterOrder));
								// Discard any dependency on stubs file
								if ((dependency.getInnerFile().equals(getStubsPath())) || (dependency.getOuterFile().equals(getStubsPath()))) {
									continue;
								}
								FindFunctionDefVisitor findFunctionDefVisitor = new FindFunctionDefVisitor();
								functionCall.accept(findFunctionDefVisitor);
								Function caller = getFunctions().get(findFunctionDefVisitor.getFunctionDef());
								((FunctionCallDependency) dependency).setCaller(caller);
								function.getDependencies().add(dependency);
								getDependencies().add(dependency);
							}
						}
					}
				}
				parameterOrder++;
			}
			System.out.println(function.getDependencies().size() + " function call dependencies found");
		}
	}

	public void findFunctions() {
		int i = 1;
		System.out.println("Looking for function definitions on ASTs (" + getAsts().size() + ")");
		for (Ast ast : getAsts()) {
			System.out.println("AST " + i++ + "/" + getAsts().size());
			Node myAst = ast.getNode();
			FindFunctionsVisitor findFunctionsVisitor = new FindFunctionsVisitor();
			myAst.accept(findFunctionsVisitor);
			// Get all function definitions on this AST
			List<FunctionDef> functionDefs = findFunctionsVisitor.getFunctions();
			System.out.println(functionDefs.size() + " function definitions found");
			// Create a function object for each function definition
			for (FunctionDef functionDef : functionDefs) {
				FindFunctionDirectivesVisitor findFunctionDirectivesVisitor = new FindFunctionDirectivesVisitor(functionDef);
				functionDef.accept(findFunctionDirectivesVisitor);
				// Get function directives
				Set<Opt> functionDirectives = findFunctionDirectivesVisitor.getFunctionDirectives();
				// Get function inner directives (ifdefs inside function)
				Set<Opt> functionOpts = findFunctionDirectivesVisitor.getFunctionOpts();
				FindFunctionParametersVisitor findFunctionParametersVisitor = new FindFunctionParametersVisitor();
				functionDef.accept(findFunctionParametersVisitor);
				// Get function parameters
				List<Id> functionParameters = findFunctionParametersVisitor.getFunctionParameters();
				Function function = new Function(functionDef, functionParameters);
				function.setDirectives(functionDirectives);
				function.setOpts(functionOpts);
				getFunctions().put(functionDef, function);
			}
		}
	}
	
	/*public int getArgumentPosition(FunctionCall functionCall, Id argument) {
		String argumentName = argument.getName();
		FindFunctionCallArgumentsVisitor findFunctionCallArgumentsVisitor = new FindFunctionCallArgumentsVisitor();
		functionCall.accept(findFunctionCallArgumentsVisitor);
		List<Node> arguments = findFunctionCallArgumentsVisitor.getFunctionCallArguments();
		int i = 0;
		for (Id arg : arguments) {
			i++;
			if (arg.getName().equals(argumentName)) {
				return i;
			}
		}
		return 0;
	}*/
	
	public Node getArgumentByPosition(FunctionCall functionCall, int position) {
		FindFunctionCallArgumentsVisitor findFunctionCallArgumentsVisitor = new FindFunctionCallArgumentsVisitor();
		functionCall.accept(findFunctionCallArgumentsVisitor);
		List<Node> arguments = findFunctionCallArgumentsVisitor.getFunctionCallArguments();
		if (arguments.size() > position) {
			return arguments.get(position);
		}
		return null;
	}
	
	public void findFunctionCalls() {
		int i = 1;
		System.out.println("Looking for function calls on ASTs (" + getAsts().size() + ")");
		for (Ast ast : getAsts()) {
			//System.out.println("AST " + i + "/" + getAsts().size());
			Node myAst = ast.getNode();
			int j = 1;
			for (Entry<FunctionDef, Function> functionMap : getFunctions().entrySet()) {
				Function function = functionMap.getValue();
				//System.out.println("AST " + i + "/" + getAsts().size() + " - Function " + j++ + "/" + getFunctions().size() + " (" + function.getName() + ")");
				interprocedural.visitor.FindFunctionCallsVisitor findFunctionCallsVisitor = new interprocedural.visitor.FindFunctionCallsVisitor(function.getFunctionDef());
				myAst.accept(findFunctionCallsVisitor);
				// Get all calls for this function
				List<FunctionCall> functionCalls = findFunctionCallsVisitor.getFunctionCalls();
				//System.out.println(functionCalls.size() + " function calls found");
				function.getFunctionCalls().addAll(functionCalls);
			}
			i++;
		}
	}
	
	public void startDFS() {
		int i = 0;
		initializeFile();
		//Dependency dependency = getDependencies().get(0);
		for (Dependency dependency : getDependencies()) {
			if (!(dependency instanceof FunctionCallDependency)) {
				//continue;
			}
			
			int position = getParameterPosition(dependency.getCallee().getFunctionDef(), dependency.getVariable());
			
			if (position == -1) {
				//continue;
			}
			
			FunctionCall functionCall = ((FunctionCallDependency) dependency).getFunctionCall();
			
			ExprList exprList = null;
			// Get function call argument list
			for (int j = 0; j < functionCall.getChildren().size(); j++) {
				if (functionCall.getChildren().get(j) instanceof ExprList) {
					exprList = (ExprList) functionCall.getChildren().get(j);
				}
			}
			if (exprList == null) {
				//continue;
			}
			if (exprList.getChildren().size() <= position) {
				//continue;
			}
			// Get the argument at position "position" in function call
			Node exprListChild = exprList.getChildren().get(position);
			FindVariablesInFunctionCallArgumentsVisitor findVariablesInFunctionCallArgumentsVisitor = new FindVariablesInFunctionCallArgumentsVisitor();
			exprListChild.accept(findVariablesInFunctionCallArgumentsVisitor);
			// Get the variables used as arguments in the function call
			List<Id> variables = findVariablesInFunctionCallArgumentsVisitor.getFunctionCallVariables();
			// Get the functionDef outside the function call
			FunctionDef functionDef = (FunctionDef) findParent(functionCall, "tree.FunctionDef");
			boolean nodeAdded = false;
			for (Id variable : variables) {
				int parameterPosition = getParameterPosition(functionDef, variable);
				// If the variable is found on the function parameter list
				if (parameterPosition != -1) {
					// Create a new graph node
					GraphNode graphNode = new GraphNode(functionCall, position, variable.getPresenceCondition());
					visitedGraphNodes = new HashSet<GraphNode>();
					//System.out.println("Dep: " + dependency + ". GraphNode: " + graphNode);
					modifiedDfs(dependency, graphNode, graphNode.getPresenceCondition().and(dependency.getPresenceCondition()), new ArrayList<GraphNode>(Arrays.asList(graphNode)));
					//dfs(dependency, graphNode, graphNode.getPresenceCondition().and(dependency.getPresenceCondition()), new ArrayList<GraphNode>(Arrays.asList(graphNode)));
					//maximumDepthNodes.put(dependency, result);
					//System.out.println("Added result: " + result);
					nodeAdded = true;
				}
			}
			if (!nodeAdded) {
				GraphNode graphNode1 = new GraphNode(functionCall, position, exprListChild.getPresenceCondition());
				GraphNode graphNode2 = new GraphNode(functions.get(functionDef).getName(), -1, functionDef.getPresenceCondition(), functionDef);
				//maximumDepthNodes.put(dependency, new ArrayList<GraphNode>(Arrays.asList(graphNode)));
				List<GraphNode> path = new ArrayList<GraphNode>();
				path.add(graphNode1);
				path.add(graphNode2);
				dependencyPaths.put(dependency, (Arrays.asList(path)));
			}
			
		}
	}

	public void generateMetrics() {
		// Get every call to a function with dependency
		//measureMaintenancePoints();
		// Get every reference to a variable which causes dependency inside a function
		//measureImpactPoints();
		// Get all chained calls to a function which causes dependency
		//measureDependencyDepths();
		// DFS
		startDFS();
	}

	
}
