package depth.core;

import tree.FunctionCall;
import tree.FunctionDef;
import tree.Id;
import tree.Node;
import de.fosd.typechef.featureexpr.FeatureExpr;

public class GraphNode {
	private FunctionCall functionCall;
	private int parameterPosition;
	private FeatureExpr presenceCondition;
	private String functionName;
	private FunctionDef functionDef;
	
	public FunctionCall getFunction() {
		return functionCall;
	}

	public void setFunctionCall(FunctionCall functionCall) {
		this.functionCall = functionCall;
	}

	public int getParameterPosition() {
		return parameterPosition;
	}

	public void setParameterPosition(int parameterPosition) {
		this.parameterPosition = parameterPosition;
	}

	public FeatureExpr getPresenceCondition() {
		return presenceCondition;
	}

	public void setPresenceCondition(FeatureExpr presenceCondition) {
		this.presenceCondition = presenceCondition;
	}
	
	public String getFunctionName() {
		if (functionName != null) {
			return functionName;
		} else if (functionCall != null) {
			return ((Id) functionCall.getParent().getChildren().get(0)).getName();
		} else {
			return "";
		}
	}
	
	public FunctionDef getFunctionDef() {
		if (functionDef != null) {
			return functionDef;
		} else {
			Node parent = functionCall;
			while (!(parent.getClass().getCanonicalName().equals("tree.FunctionDef"))) {
				parent = parent.getParent();
				if (parent == null) {
					break;
				}
			}
			return (FunctionDef) parent;
		}
	}

	public void setFunctionDef(FunctionDef functionDef) {
		this.functionDef = functionDef;
	}

	public GraphNode(FunctionCall functionCall, int parameterPosition,
			FeatureExpr presenceCondition) {
		this.functionCall = functionCall;
		this.parameterPosition = parameterPosition;
		this.presenceCondition = presenceCondition;
	}
	
	public GraphNode(String functionName, int parameterPosition,
			FeatureExpr presenceCondition, FunctionDef functionDef) {
		this.functionName = functionName;
		this.parameterPosition = parameterPosition;
		this.presenceCondition = presenceCondition;
		this.functionDef = functionDef;
	}
	
	public boolean equals(Object other) {
		if (other == this) return true;
		if (other == null) return false;
		if (getClass() != other.getClass()) return false;
		GraphNode graphNode = (GraphNode) other;
		return (
			(getParameterPosition() == graphNode.getParameterPosition()) &&
			(getFunctionName().equals(graphNode.getFunctionName())) && 
			(getPresenceCondition().equivalentTo(graphNode.getPresenceCondition()))
		);
	}
	
	public int hashCode() {
		return getFunctionName().hashCode() ^ getPresenceCondition().hashCode() ^ (getParameterPosition() * 31);
	}
	
	public String toString() {
		return getFunctionName() + ", " + getParameterPosition() + ", " + getPresenceCondition();
	}
	
}
